from collections import deque, defaultdict, Counter


def process_pair(char, char2, iters):
    if (char, char2, iters) in cache:
        return cache[(char, char2, iters)]
    if iters == 0:
        return {}
    pre_dict = process_pair(char, instructions[char + char2], iters - 1)
    post_dict = process_pair(instructions[char + char2], char2, iters - 1)
    retval = dict(Counter(pre_dict) + Counter({instructions[char + char2]: 1}) + Counter(post_dict))
    cache[(char, char2, iters)] = retval
    return retval


instructions = defaultdict(str)
with open("day14.txt", "r") as fd:
    seed = deque(fd.readline().strip())
    for line in fd:
        if line.strip() == "":
            continue
        code, insertee = line.strip().split("->")
        instructions[code.strip()] = insertee.strip()

count, cache = {seed[0]: 1}, {}
for pos in range(1, len(seed)):
    count = dict(Counter(count) + Counter(process_pair(seed[pos - 1], seed[pos], 40)) + Counter({seed[pos]: 1}))
print(max([v for v in count.values()]) - min([v for v in count.values()]))
