from math import prod

tranlations: dict = {"0": "0000", "1": "0001", "2": "0010", "3": "0011", "4": "0100", "5": "0101", "6": "0110",
                     "7": "0111", "8": "1000", "9": "1001", "A": "1010", "B": "1011", "C": "1100", "D": "1101",
                     "E": "1110", "F": "1111"}
my_bin_str: [str] = []
with open("day16.txt", "r") as fd:
    for char in fd.readline().strip():
        my_bin_str += list(tranlations[char])


class Packet:
    def __init__(self, version: str, sub_type: str):
        self.version_bin: str = version
        self.version: int = int(version, 2)
        self.sub_type_bin: str = sub_type
        self.sub_type: int = int(sub_type, 2)
        self.payload: int = 0
        self.subs: [Packet] = []

    def evaluate(self) -> int:
        if self.sub_type == 0:
            return sum([x.evaluate() for x in self.subs])
        if self.sub_type == 1:
            return prod([x.evaluate() for x in self.subs])
        if self.sub_type == 2:
            return min([x.evaluate() for x in self.subs])
        if self.sub_type == 3:
            return max([x.evaluate() for x in self.subs])
        if self.sub_type == 4:
            return self.payload
        if self.sub_type == 5:
            return 1 if self.subs[0].evaluate() > self.subs[1].evaluate() else 0
        if self.sub_type == 6:
            return 1 if self.subs[0].evaluate() < self.subs[1].evaluate() else 0
        if self.sub_type == 7:
            return 1 if self.subs[0].evaluate() == self.subs[1].evaluate() else 0


class Literal(Packet):
    def __init__(self, version: str, sub_type: str, payload: str):
        super().__init__(version, sub_type)
        self.payload_bin: str = payload
        self.payload: int = int(payload, 2)


class Operator(Packet):
    def __init__(self, version: str, sub_type: str, subs: [Packet]):
        super().__init__(version, sub_type)
        self.subs: [Packet] = subs


def parse_sub_packet(bin_str: [str]) -> ([str], Packet):
    version: str = ""
    sub_type: str = ""
    for _ in range(3):
        version += bin_str.pop(0)
    for _ in range(3):
        sub_type += bin_str.pop(0)
    if int(sub_type, 2) == 4:
        payload: str = ""
        last_byte: bool = False
        while not last_byte:
            section: str = ""
            for _ in range(5):
                section += bin_str.pop(0)
            if section[0] == "0":
                last_byte = True
            payload += section[1:]
        sub_packet: Literal = Literal(version, sub_type, payload)
    else:
        len_type: str = bin_str.pop(0)
        if len_type == "0":
            sub_str: [str] = []
            length_in_bits: str = ""
            for _ in range(15):
                length_in_bits += bin_str.pop(0)
            for _ in range(int(length_in_bits, 2)):
                sub_str += list(bin_str.pop(0))
            subs: [Packet] = []
            sub_str = list(sub_str)
            while sub_str:
                sub_str, sub = parse_sub_packet(sub_str)
                subs.append(sub)
            sub_packet: Operator = Operator(version, sub_type, subs)
        else:
            length_in_subs: str = ""
            for _ in range(11):
                length_in_subs += bin_str.pop(0)
            subs = []
            while len(subs) < int(length_in_subs, 2):
                bin_str, sub = parse_sub_packet(bin_str)
                subs.append(sub)
            sub_packet: Operator = Operator(version, sub_type, subs)
    return bin_str, sub_packet


blocks: [Packet] = []
while my_bin_str:
    if all([x == "0" for x in my_bin_str]):
        break
    my_bin_str, packet = parse_sub_packet(my_bin_str)
    blocks.append(packet)
print(blocks[0].evaluate())
