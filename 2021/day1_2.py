content_list = []
with open("day1.txt", "r") as fd:
    for x in fd:
        content_list.append(int(x))
sum_list = []
for y in range(2, len(content_list)):
    sum_list.append(content_list[y] + content_list[y - 1] + content_list[y - 2])

print(sum_list)

last = -1
counter = 0
for x in sum_list:
    print(f"x={x}, last={last}")
    if last == -1:
        last = x
        continue
    if x > last:
        counter += 1
    last = x

print(counter)
