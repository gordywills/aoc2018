countdict = {}
length = 0
with open("day3.txt", "r") as fd:
    for x in fd:
        length += 1
        for i, v in enumerate(x.strip()):
            if i in countdict:
                countdict[i] += int(v)
            else:
                countdict[i] = int(v)
gamma = []
episilon = []
for val in countdict.values():
    if val > (length / 2):
        gamma.append("1")
        episilon.append("0")
    else:
        gamma.append("0")
        episilon.append("1")
print(int("".join(gamma), 2) * int("".join(episilon), 2))
