class Cave:
    def __init__(self, name: str, is_big: bool):
        self.name: str = name
        self.is_big: bool = is_big
        self.links_to: list = []

    def set_link(self, name: str):
        self.links_to.append(name)

    def get_size(self):
        return self.is_big

    def get_links(self):
        return self.links_to


def find_routes(start: str, end: str, path: list, all_paths: list):
    path.append(start)
    if start == end:
        all_paths.append(path.copy())
        return
    for link in all_caves[start].get_links():
        if all_caves[link].is_big or (link not in path):
            find_routes(link, end, path.copy(), all_paths)


all_caves = {}
with open("day12.txt", "r") as fd:
    for line in fd:
        left, right = line.strip().split("-")
        if left not in all_caves:
            all_caves[left] = Cave(left, left.isupper())
        if right not in all_caves:
            all_caves[right] = Cave(right, right.isupper())
        all_caves[left].set_link(right)
        all_caves[right].set_link(left)

all_paths = []
routes = find_routes("start", "end", [], all_paths)
print(len(all_paths))
