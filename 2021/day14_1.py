from collections import deque, Counter, defaultdict


def process_polymer(pre_seed: str):
    post_seed = deque()
    post_seed.append(pre_seed[0])
    for x in range(1, len(pre_seed)):
        post_seed.append(instructions[pre_seed[x - 1] + pre_seed[x]])
        post_seed.append(pre_seed[x])
    return post_seed


instructions = defaultdict(str)
with open("day14.txt", "r") as fd:
    seed = deque(fd.readline().strip())
    for line in fd:
        if line.strip() == "":
            continue
        code, insertee = line.strip().split("->")
        instructions[code.strip()] = insertee.strip()

seeds = defaultdict(list)
seeds[0] = seed
for iteration in range(1, 11):
    seed = process_polymer(seed)
    seeds[iteration] = seed

element_count = Counter(seed)
print(max([v for v in element_count.values()]) - min([v for v in element_count.values()]))
