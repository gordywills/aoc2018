from collections import defaultdict
from typing import Dict


def fold_up_at(paper: Dict[tuple, str], fold: int) -> Dict[tuple, str]:
    max_x: int = max([c[0] for c in paper.keys()]) + 1
    new_paper: Dict[tuple, str] = defaultdict(str)
    for x in range(max_x):
        for y in range(fold):
            if paper[(x, y)] == "#" or paper[(x, (fold * 2) - y)] == "#":
                new_paper[(x, y)] = "#"
    return new_paper


def fold_left_at(paper: Dict[tuple, str], fold: int) -> Dict[tuple, str]:
    max_y: int = max([c[1] for c in paper.keys()]) + 1
    new_paper: Dict[tuple, str] = defaultdict(str)
    for x in range(fold):
        for y in range(max_y):
            if paper[(x, y)] == "#" or paper[((fold * 2) - x, y)] == "#":
                new_paper[(x, y)] = "#"
    return new_paper


def print_paper(paper: Dict[tuple, str]):
    max_x: int = max([c[0] for c in paper.keys()]) + 1
    max_y: int = max([c[1] for c in paper.keys()]) + 1
    for y in range(max_y):
        for x in range(max_x):
            if (x, y) in paper.keys():
                print(paper[(x, y)], end="")
            else:
                print(" ", end="")
        print("")


paper: Dict[tuple, str] = defaultdict(str)
instructions: list = []
with open("day13.txt", "r") as fd:
    for line in fd:
        if line.strip() == "":
            break
        x, y = line.strip().split(",")
        paper[(int(x), int(y))] = "#"
    for line in fd:
        axis, val = line.strip().split("=")
        instructions.append((axis[-1:], int(val)))

function_lookup: Dict[str, callable] = {"x": fold_left_at, "y": fold_up_at}
paper = function_lookup[instructions[0][0]](paper, instructions[0][1])
print(len(paper))
