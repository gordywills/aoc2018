from pathfinding.core.diagonal_movement import DiagonalMovement
from pathfinding.core.grid import Grid
from pathfinding.finder.a_star import AStarFinder

matrix = []
with open("day15.txt", "r") as fd:
    for line in fd:
        matrix.append(list(map(int, list(line.strip()))))

grid = Grid(matrix=matrix)

start = grid.node(0, 0)
end = grid.node(len(matrix[0]) - 1, len(matrix) - 1)

finder = AStarFinder(diagonal_movement=DiagonalMovement.never)
path, _ = finder.find_path(start, end, grid)
total = 0
for x in path[1:]:
    total += matrix[x[1]][x[0]]

print(total)
