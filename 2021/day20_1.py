from collections import defaultdict

image = defaultdict(lambda: 0)

with open("day20.txt", "r") as fd:
    algo = [1 if x == "#" else 0 for x in list(fd.readline().strip())]
    _ = fd.readline()
    linecount = 0
    for line in fd:
        char_count = 0
        for char in line.strip():
            image[(char_count, linecount)] = 1 if char == "#" else 0
            char_count += 1
        linecount += 1
final_space = 0


def get_min(image_tp_process, tuple_pos):
    return min([x[tuple_pos] for x, v in image_tp_process.items()])


def get_max(image_tp_process, tuple_pos):
    return max([x[tuple_pos] for x, v in image_tp_process.items()])


def get_total(image_to_process):
    return sum([v for x, v in image_to_process.items()])


def print_image(image_to_print):
    left, right = get_min(image_to_print, 0) - 2, get_max(image_to_print, 0) + 3
    up, down = get_min(image_to_print, 1) - 2, get_max(image_to_print, 1) + 3
    for y in range(up, down):
        for x in range(left, right):
            if image_to_print[(x, y)] == 1:
                print("#", end="")
            else:
                print(".", end="")
        print("")


def process_image(image_to_process, this_final_space):
    left, right = get_min(image_to_process, 0), get_max(image_to_process, 0) + 1
    up, down = get_min(image_to_process, 1), get_max(image_to_process, 1) + 1
    for x in range(left, right):
        image_to_process[(x, down + 2)] = this_final_space
        image_to_process[(x, down + 1)] = this_final_space
        image_to_process[(x, up - 1)] = this_final_space
        image_to_process[(x, up - 2)] = this_final_space
    for y in range(up - 2, down + 2):
        image_to_process[(left - 2, y)] = this_final_space
        image_to_process[(left - 1, y)] = this_final_space
        image_to_process[(right + 1, y)] = this_final_space
        image_to_process[(right + 2, y)] = this_final_space
    this_final_space = algo[0] if this_final_space == 0 else algo[511]
    new_image = defaultdict(lambda: this_final_space)
    for y in range(up - 2, down + 2):
        for x in range(left - 2, right + 2):
            binary_str = int(
                str(image_to_process[(x - 1, y - 1)]) +
                str(image_to_process[(x, y - 1)]) +
                str(image_to_process[(x + 1, y - 1)]) +
                str(image_to_process[(x - 1, y)]) +
                str(image_to_process[(x, y)]) +
                str(image_to_process[(x + 1, y)]) +
                str(image_to_process[(x - 1, y + 1)]) +
                str(image_to_process[(x, y + 1)]) +
                str(image_to_process[(x + 1, y + 1)])
                , 2)
            new_image[(x, y)] = algo[binary_str]
    return new_image, this_final_space


for x in range(2):
    image, final_space = process_image(image, final_space)

print(get_total(image.copy()))
