import re

pattern = re.compile(r".=(\-?\d*)\.\.(\-?\d*).*=(\-?\d*)\.\.(\-?\d*)")
with open("day17.txt", "r") as fd:
    matches = pattern.search(fd.readline())
    min_x, max_x, max_y, min_y = int(matches[1]), int(matches[2]), int(matches[4]), int(matches[3])
y_velocity = abs(min_y) - 1
height = int((y_velocity * (y_velocity + 1)) / 2)
print(height)
