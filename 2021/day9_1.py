with open("day9.txt", "r") as fd:
    puzzle_space = []
    for line in fd:
        line_list = [9]
        line_list += list(map(int, [x for x in line.strip()]))
        line_list.append(9)
        puzzle_space.append(line_list)

boarder = [9] * len(puzzle_space[0])
puzzle_space.append(boarder)
puzzle_space.insert(0, boarder)

low_points = []
for x in range(1, len(puzzle_space[0]) - 1):
    for y in range(1, len(puzzle_space) - 1):
        if puzzle_space[y][x] >= puzzle_space[y][x - 1] \
                or puzzle_space[y][x] >= puzzle_space[y][x + 1] \
                or puzzle_space[y][x] >= puzzle_space[y - 1][x] \
                or puzzle_space[y][x] >= puzzle_space[y + 1][x]:
            continue
        low_points.append(puzzle_space[y][x])

print(sum(low_points) + len(low_points))
