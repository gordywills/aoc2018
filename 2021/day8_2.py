puzzle_list = []
with open("day8.txt", "r") as fd:
    for line in fd:
        inputs, outputs = line.strip().split("|")
        input_list = inputs.strip().split(" ")
        output_list = outputs.strip().split(" ")
        puzzle_list.append([input_list, output_list])
total = 0
for puzzle in puzzle_list:
    solution_dict = {0: '', 1: '', 2: '', 3: '', 4: '', 5: '', 6: '', 7: '', 8: '', 9: ''}
    for x in puzzle[0]:
        if len(x) == 2:
            solution_dict[1] = "".join(sorted(x))
        if len(x) == 3:
            solution_dict[7] = "".join(sorted(x))
        if len(x) == 4:
            solution_dict[4] = "".join(sorted(x))
        if len(x) == 7:
            solution_dict[8] = "".join(sorted(x))
    sixes_list = [x for x in puzzle[0] if len(x) == 6]
    for x in sixes_list:
        a = set(solution_dict[1])
        b = set(x)
        if len(a - b) == 1:
            solution_dict[6] = "".join(sorted(x))
            sixes_list.remove(x)
            break
    for x in sixes_list:
        a = set(solution_dict[4])
        b = set(x)
        if len(a - b) == 1:
            solution_dict[0] = "".join(sorted(x))
        if len(a - b) == 0:
            solution_dict[9] = "".join(sorted(x))
    fives_list = [x for x in puzzle[0] if len(x) == 5]
    for x in fives_list:
        a = set(solution_dict[6])
        b = set(x)
        if len(a - b) == 1:
            solution_dict[5] = "".join(sorted(x))
            fives_list.remove(x)
            break
    for x in fives_list:
        a = set(solution_dict[9])
        b = set(x)
        if len(a - b) == 1:
            solution_dict[3] = "".join(sorted(x))
        if len(a - b) == 2:
            solution_dict[2] = "".join(sorted(x))
    number = []
    solution_dict = {v: i for i, v in solution_dict.items()}
    for o in puzzle[1]:
        number.append(solution_dict["".join(sorted(o))])
    total += (int("".join([str(x) for x in number])))

print(total)
