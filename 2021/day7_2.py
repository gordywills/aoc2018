import numpy as np

with open("day7.txt", "r") as fd:
    raw_list = fd.readline().strip().split(",")
    processed_list = list(map(int, raw_list))
found = False
maximum = np.max(processed_list)
minimum = 0
middle = round(np.mean(processed_list))
while found == False:
    costm = round(np.sum([(np.square(abs(middle - x)) + (abs(middle - x))) / 2 for x in processed_list]))
    costu = round(np.sum([(np.square(abs(middle + 1 - x)) + (abs(middle + 1 - x))) / 2 for x in processed_list]))
    costd = round(np.sum([(np.square(abs(middle - 1 - x)) + (abs(middle - 1 - x))) / 2 for x in processed_list]))
    if costd > costm < costu:
        found = True
        continue
    if costd < costm < costu:
        maximum = middle
        middle = round((maximum + minimum) / 2)
        continue
    if costd > costm > costu:
        minimum = middle
        middle = round((maximum + minimum) / 2)
        continue

print(costm)
