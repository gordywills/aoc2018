def laternfish_loop(days):
    if days == 0:
        return 6
    return days - 1


with open("day6.txt", "r") as fd:
    raw_list = fd.readline().strip().split(",")
    processed_list = list(map(int, raw_list))

for x in range(18):
    zeros = processed_list.count(0)
    processed_list = list(map(laternfish_loop, processed_list))
    for y in range(zeros):
        processed_list.append(8)

print(len(processed_list))
