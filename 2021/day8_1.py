puzzle_list = []
outputs_list = []
inputs_list = []
with open("day8.txt", "r") as fd:
    for line in fd:
        inputs, outputs = line.strip().split("|")
        input_list = inputs.strip().split(" ")
        output_list = outputs.strip().split(" ")
        puzzle_list.append([input_list, output_list])
        inputs_list.append(input_list)
        outputs_list.append(output_list)

total = 0
for out in outputs_list:
    total += sum([1 for x in out if len(x) in (2, 4, 3, 7)])
print(total)
