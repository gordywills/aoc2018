def basin_crawler(set_so_far, puzz_spac, loc):
    set_so_far.add(loc)
    if puzz_spac[loc[0] - 1][loc[1]] != 9 and (loc[0] - 1, loc[1]) not in set_so_far:
        set_so_far.union(basin_crawler(set_so_far, puzz_spac, (loc[0] - 1, loc[1])))
    if puzz_spac[loc[0] + 1][loc[1]] != 9 and (loc[0] + 1, loc[1]) not in set_so_far:
        set_so_far.union(basin_crawler(set_so_far, puzz_spac, (loc[0] + 1, loc[1])))
    if puzz_spac[loc[0]][loc[1] - 1] != 9 and (loc[0], loc[1] - 1) not in set_so_far:
        set_so_far.union(basin_crawler(set_so_far, puzz_spac, (loc[0], loc[1] - 1)))
    if puzz_spac[loc[0]][loc[1] + 1] != 9 and (loc[0], loc[1] + 1) not in set_so_far:
        set_so_far.union(basin_crawler(set_so_far, puzz_spac, (loc[0], loc[1] + 1)))
    return set_so_far


with open("day9.txt", "r") as fd:
    puzzle_space = []
    for line in fd:
        line_list = [9]
        line_list += list(map(int, [x for x in line.strip()]))
        line_list.append(9)
        puzzle_space.append(line_list)

boarder = [9] * len(puzzle_space[0])
puzzle_space.append(boarder)
puzzle_space.insert(0, boarder)

low_points = []
for x in range(1, len(puzzle_space[0]) - 1):
    for y in range(1, len(puzzle_space) - 1):
        if puzzle_space[y][x] >= puzzle_space[y][x - 1] \
                or puzzle_space[y][x] >= puzzle_space[y][x + 1] \
                or puzzle_space[y][x] >= puzzle_space[y - 1][x] \
                or puzzle_space[y][x] >= puzzle_space[y + 1][x]:
            continue
        low_points.append((y, x))

basins = {}
for low_point in low_points:
    basin = basin_crawler(set(), puzzle_space, low_point)
    basins[low_point] = (len(basin))

while len(basins) > 3:
    basins = {k: v for k, v in basins.items() if v > min(basins.values())}

total = 1
for x in basins.values():
    total *= x

print(total)
