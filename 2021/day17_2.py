import re

pattern = re.compile(r".=(-?\d*)\.\.(-?\d*).*=(-?\d*)\.\.(-?\d*)")
with open("day17.txt", "r") as fd:
    matches = pattern.search(fd.readline())
    min_x, max_x, max_y, min_y = int(matches[1]), int(matches[2]), int(matches[4]), int(matches[3])

possible_values_of_x = []
for x in range(1, max_x + 1):
    counter, distance, velocity = 1, x, x - 1
    while distance < max_x + 1:
        if min_x <= distance <= max_x:
            possible_values_of_x.append((x, counter, velocity == 0))
        distance += velocity
        counter += 1
        if velocity == 0:
            break
        velocity -= 1
possible_values_of_y = []
for y in range(min_y, abs(min_y)):
    counter, distance, velocity = 1, y, y - 1
    while distance > min_y - 1:
        if min_y <= distance <= max_y:
            possible_values_of_y.append((y, counter))
        distance += velocity
        counter += 1
        velocity -= 1
possible_velocities = set()
for x_value in possible_values_of_x:
    for y_value in possible_values_of_y:
        if (x_value[2] and y_value[1] >= x_value[1]) or (y_value[1] == x_value[1]):
            possible_velocities.add((x_value[0], y_value[0]))
print(len(possible_velocities))
