import re
from collections import defaultdict

regx = re.compile(r"(\d*),(\d*) -> (\d*),(\d*)")
grid = defaultdict(lambda: 0)
coords_list = []
with open("day5.txt", "r") as fd:
    for line in fd:
        matches = regx.fullmatch(line.strip())
        coords_list.append(((int(matches[1]), int(matches[2])), (int(matches[3]), int(matches[4]))))

for pair in coords_list:
    if pair[0][0] != pair[1][0] and pair[0][1] != pair[1][1]:
        continue
    if pair[0][0] == pair[1][0]:
        for x in range(min([pair[0][1], pair[1][1]]), max([pair[0][1], pair[1][1]]) + 1):
            grid[(pair[0][0], x)] += 1
    if pair[0][1] == pair[1][1]:
        for x in range(min([pair[0][0], pair[1][0]]), max([pair[0][0], pair[1][0]]) + 1):
            grid[(x, pair[0][1])] += 1
count = 0
for x in grid.values():
    if x > 1:
        count += 1
print(count)
