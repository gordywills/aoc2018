countdict = {}
baselist = []
with open("day3.txt", "r") as fd:
    for x in fd:
        baselist.append(x.strip())

oxylist = baselist.copy()
colist = baselist.copy()
for y in range(len(baselist[0])):
    counter = 0
    newoxylist = []
    if len(oxylist) == 1:
        continue
    for x in oxylist:
        if x[y] == "1":
            counter += 1
    if counter >= (len(oxylist) / 2):
        for x in oxylist:
            if x[y] == "1":
                newoxylist.append(x)
    else:
        for x in oxylist:
            if x[y] == "0":
                newoxylist.append(x)
    oxylist = newoxylist.copy()

for y in range(len(baselist[0])):
    counter = 0
    newcolist = []
    if len(colist) == 1:
        continue
    for x in colist:
        if x[y] == "1":
            counter += 1
    if counter >= (len(colist) / 2):
        for x in colist:
            if x[y] == "0":
                newcolist.append(x)
    else:
        for x in colist:
            if x[y] == "1":
                newcolist.append(x)
    colist = newcolist.copy()

print(int("".join(oxylist), 2) * int("".join(colist), 2))
