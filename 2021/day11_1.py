with open("day11.txt", "r") as fd:
    grid = [[-1] + [int(octo) for octo in line.strip()] + [-1] for line in fd]
grid.append([-1] * len(grid[0]))
grid.insert(0, [-1] * len(grid[0]))


def increment_neighbours(home, grid):
    if grid[home[0] - 1][home[1] - 1] != -1:
        grid[home[0] - 1][home[1] - 1] += 1
    if grid[home[0] - 1][home[1]] != -1:
        grid[home[0] - 1][home[1]] += 1
    if grid[home[0] - 1][home[1] + 1] != -1:
        grid[home[0] - 1][home[1] + 1] += 1
    if grid[home[0]][home[1] - 1] != -1:
        grid[home[0]][home[1] - 1] += 1
    if grid[home[0]][home[1] + 1] != -1:
        grid[home[0]][home[1] + 1] += 1
    if grid[home[0] + 1][home[1] - 1] != -1:
        grid[home[0] + 1][home[1] - 1] += 1
    if grid[home[0] + 1][home[1]] != -1:
        grid[home[0] + 1][home[1]] += 1
    if grid[home[0] + 1][home[1] + 1] != -1:
        grid[home[0] + 1][home[1] + 1] += 1
    return grid


def increment_step(grid):
    for x in range(1, len(grid[0]) - 1):
        for y in range(1, len(grid) - 1):
            grid[x][y] += 1
    return grid


def reset_grid(grid):
    for x in range(1, len(grid[0]) - 1):
        for y in range(1, len(grid) - 1):
            if grid[x][y] > 9:
                grid[x][y] = 0
    return grid


def search_for_flashes(flashed, grid):
    triggered = True
    while triggered:
        triggered = False
        for x in range(1, len(grid[0]) - 1):
            for y in range(1, len(grid) - 1):
                if grid[x][y] > 9 and (x, y) not in flashed:
                    flashed.add((x, y))
                    triggered = True
                    grid = increment_neighbours((x, y), grid)
    return flashed, grid


total = 0

for x in range(100):
    grid = increment_step(grid)
    flashed_this_step, grid = search_for_flashes(set(), grid)
    total += len(flashed_this_step)
    grid = reset_grid(grid)

print(total)
