from pathfinding.core.diagonal_movement import DiagonalMovement
from pathfinding.core.grid import Grid
from pathfinding.finder.a_star import AStarFinder

matrix = []
with open("day15.txt", "r") as fd:
    for line in fd:
        matrix.append(list(map(int, list(line.strip()))))
new_matrix = []
for line in matrix:
    new_matrix.append(
        line +
        list(map(lambda x: 9 if x + 1 == 9 else (x + 1) % 9, line)) +
        list(map(lambda x: 9 if x + 2 == 9 else (x + 2) % 9, line)) +
        list(map(lambda x: 9 if x + 3 == 9 else (x + 3) % 9, line)) +
        list(map(lambda x: 9 if x + 4 == 9 else (x + 4) % 9, line))
    )
new_new_matrix = []
for y in range(5):
    for x in range(len(new_matrix)):
        new_new_matrix.append(list(map(lambda x: 9 if x + y == 9 else (x + y) % 9, new_matrix[x])))
grid = Grid(matrix=new_new_matrix)
finder = AStarFinder(diagonal_movement=DiagonalMovement.never)
path, _ = finder.find_path(grid.node(0, 0), grid.node(len(new_new_matrix[0]) - 1, len(new_new_matrix) - 1), grid)
total = 0
for x in path[1:]:
    total += new_new_matrix[x[1]][x[0]]
print(total)
