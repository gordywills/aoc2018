tranlations = {"0": "0000", "1": "0001", "2": "0010", "3": "0011", "4": "0100", "5": "0101", "6": "0110", "7": "0111",
               "8": "1000", "9": "1001", "A": "1010", "B": "1011", "C": "1100", "D": "1101", "E": "1110", "F": "1111"}
my_bin_str = ""
with open("day16.txt", "r") as fd:
    for char in fd.readline().strip():
        my_bin_str += tranlations[char]
my_bin_str = list(my_bin_str)

global_version_total = 0


class Literal:
    def __init__(self, version, sub_type, payload):
        self.version_bin = version
        self.version = int(version, 2)
        self.sub_type_bin = sub_type
        self.sub_type = int(sub_type, 2)
        self.payload_bin = payload
        self.payload = int(payload, 2)


class Operator:
    def __init__(self, version, sub_type, subs):
        self.version_bin = version
        self.version = int(version, 2)
        self.sub_type_bin = sub_type
        self.sub_type = int(sub_type, 2)
        self.subs = subs


def parse_sub_packet(bin_str):
    global global_version_total
    version = ""
    sub_type = ""
    for _ in range(3):
        version += bin_str.pop(0)
    global_version_total += int(version, 2)
    for _ in range(3):
        sub_type += bin_str.pop(0)
    if int(sub_type, 2) == 4:
        payload = ""
        last_byte = False
        while not last_byte:
            section = ""
            for _ in range(5):
                section += bin_str.pop(0)
            if section[0] == "0":
                last_byte = True
            payload += section[1:]
        sub_packet = Literal(version, sub_type, payload)
    else:
        len_type = bin_str.pop(0)
        if len_type == "0":
            sub_str = ""
            length_in_bits = ""
            for _ in range(15):
                length_in_bits += bin_str.pop(0)
            for _ in range(int(length_in_bits, 2)):
                sub_str += bin_str.pop(0)
            subs = []
            sub_str = list(sub_str)
            while sub_str:
                sub_str, sub = parse_sub_packet(sub_str)
                subs.append(sub)
            sub_packet = Operator(version, sub_type, subs)
        else:
            length_in_subs = ""
            for _ in range(11):
                length_in_subs += bin_str.pop(0)
            subs = []
            while len(subs) < int(length_in_subs, 2):
                bin_str, sub = parse_sub_packet(bin_str)
                subs.append(sub)
            sub_packet = Operator(version, sub_type, subs)
    return bin_str, sub_packet


blocks = []

while my_bin_str:
    if all([x == "0" for x in my_bin_str]):
        break
    my_bin_str, packet = parse_sub_packet(my_bin_str)
    blocks.append(packet)

print(global_version_total)
