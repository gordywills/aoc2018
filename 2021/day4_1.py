from copy import deepcopy


class Board:
    def __init__(self):
        self.lines = {}

    def add_line(self, index, line_to_add):
        chars_raw = line_to_add.split(" ")
        line_of_tuples = [[int(char), 0] for char in chars_raw if char != ""]
        self.lines[index] = line_of_tuples

    def get_lines(self):
        return self.lines

    def mark_number(self, number_called):
        for line in self.lines.values():
            for line_entry in line:
                if line_entry[0] == number_called:
                    line_entry[1] = 1

    def check_line_win(self, line_number):
        for line_entry in self.lines[line_number]:
            if line_entry[1] == 0:
                return False
        return True

    def check_col_win(self, col_number):
        for x in range(5):

            if self.lines[x][col_number][1] == 0:
                return False
        return True

    def check_winner(self):
        for x in range(5):
            if self.check_line_win(x) or self.check_col_win(x):
                return True
        return False

    def get_score(self, number_called):
        total = 0
        for line in self.lines.values():
            for line_entry in line:
                if line_entry[1] == 0:
                    total += line_entry[0]
        return total * number_called


boards = []
with open("day4.txt", "r") as fd:
    file_end = False
    call_numbers = fd.readline().strip().split(",")
    call_numbers = [int(x) for x in call_numbers]
    while fd.read(1):
        new_board = Board()
        for x in range(5):
            new_board.add_line(x, fd.readline().strip())
        boards.append(deepcopy(new_board))

for called_number in call_numbers:
    for board in boards:
        board.mark_number(called_number)
        if board.check_winner():
            print(board.get_score(called_number))
            exit()
