last = -1
counter = 0
with open("day1.txt", "r") as fd:
    for x in fd:
        if int(x) > last != -1:
            counter += 1
        last = int(x)
print(counter)
