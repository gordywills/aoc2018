forward = 0
ups = 0
downs = 0
with open("day2.txt", "r") as fd:
    for x in fd:
        (direction, amount) = x.split(' ')
        print(direction, amount)
        if direction.strip() == "forward":
            forward += int(amount.strip())
        if direction.strip() == "up":
            ups += int(amount.strip())
        if direction.strip() == "down":
            downs += int(amount.strip())
print(forward, downs - ups, forward * (downs - ups))
