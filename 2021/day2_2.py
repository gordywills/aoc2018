forward = 0
depth = 0
aim = 0
with open("day2.txt", "r") as fd:
    for x in fd:
        (direction, amount) = x.split(' ')
        print(direction, amount)
        if direction.strip() == "forward":
            forward += int(amount.strip())
            depth += aim * int(amount.strip())
        if direction.strip() == "up":
            aim -= int(amount.strip())
        if direction.strip() == "down":
            aim += int(amount.strip())
print(forward, depth, forward * depth)
