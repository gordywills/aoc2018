fsh = {0: 0, 1: 0, 2: 0, 3: 0, 4: 0, 5: 0, 6: 0, 7: 0, 8: 0}
with open("day6.txt", "r") as fd:
    raw_list = fd.readline().strip().split(",")
    for x in raw_list:
        fsh[int(x.strip())] += 1

for x in range(256):
    fsh = {0: fsh[1], 1: fsh[2], 2: fsh[3], 3: fsh[4], 4: fsh[5], 5: fsh[6], 6: fsh[7] + fsh[0], 7: fsh[8], 8: fsh[0]}
    if x == 79 or x == 255: print(x + 1, sum([x for x in fsh.values()]))
