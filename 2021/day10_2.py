get_score = lambda ls, c: (ls * 5) + {"(": 1, "[": 2, "{": 3, "<": 4}[c]
totals = []
with open("day10.txt", "r") as fd:
    for line in [x.strip() for x in fd]:
        stack, line_score, corrupt = [], 0, False
        for char in line:
            if char in ["(", "{", "[", "<"]:
                stack.append(char)
                continue
            compare: str = stack.pop()
            if (char, compare) not in [(")", "("), ("}", "{"), ("]", "["), (">", "<")]:
                corrupt = True
                break
        if corrupt:
            continue
        for left_over in stack[::-1]:
            line_score = get_score(line_score, left_over)
        totals.append(line_score)
print(sorted(totals)[len(totals) // 2])
