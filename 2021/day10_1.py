get_score = lambda c: {")": 3, "}": 1197, "]": 57, ">": 25137}[c]
total = 0
with open("day10.txt", "r") as fd:
    for line in [x.strip() for x in fd]:
        stack = []
        for char in line:
            if char in ["(", "{", "[", "<"]:
                stack.append(char)
                continue
            compare: str = stack.pop()
            if (char, compare) in [(")", "("), ("}", "{"), ("]", "["), (">", "<")]:
                continue
            total += get_score(char)
            break
print(total)
