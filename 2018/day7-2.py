import re

reg = re.compile("Step (?P<parent>.) .* step (?P<child>.) ")
raw_step = [line.strip() for line in open("day7.txt", "r")]
steps = {}
letters = set()
for instruction in raw_step:
    matches = reg.match(instruction)
    temp = steps.get(matches.group("child"),[])
    temp.append(matches.group("parent"))
    steps[matches.group("child")] = temp
    letters.add(matches.group("child"))
    letters.add(matches.group("parent"))
for letter in letters - set(steps.keys()):
    steps[letter] = []
steps = {key: steps[key] for key in sorted(steps)}
time_taken = 0
workers = {0: None,1:None, 2:None, 3:None, 4:None}
while len(steps) > 0:
    next_steps = ([key for key, value in steps.items() if value == []])
    for worker_id, job in workers.items():
        for step in next_steps:
            if job is None and step not in [job[0] for id, job in workers.items() if workers[id] is not None]:
                workers[worker_id] = (step, 60 + ord(step)-64)
                next_steps.remove(step)
                break
    shortest_worker = []
    shortest_job = 90
    complete_job = []
    for worker_id, worker in workers.items():
        if worker is not None and worker[1] < shortest_job:
            shortest_job = worker[1]
            shortest_worker = [worker_id]
            complete_job=[worker[0]]
        elif worker is not None and worker[1] == shortest_job:
            shortest_worker.append(worker_id)
            complete_job.append(worker[0])
    for x in shortest_worker:
        workers[x] = None
    time_taken = time_taken + shortest_job
    for worker_id, worker in workers.items():
        if worker is not None:
            workers[worker_id] = (worker[0], worker[1] - shortest_job)
    for x in complete_job:
        del steps[x]
        for key, value in steps.items():
            if x in value:
                value.remove(x)
longest_job = 0
for worker_id, worker in workers.items():
    if worker is not None and worker[1] > longest_job:
        longest_job = worker[1]
time_taken = time_taken + longest_job
print(time_taken)
