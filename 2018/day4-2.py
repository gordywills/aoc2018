import re

shifts_raw = [line.strip() for line in open("day4.txt", "r")]
reg = re.compile("\[(?P<datetime>\d{4}-\d{2}-\d{2} \d{2}:\d{2})] (?P<message>.*)")
guard_id_reg = re.compile("#(?P<id>\d*)")
shifts = {}
for shift_raw in shifts_raw:
    matches = reg.match(shift_raw)
    shifts[matches.group("datetime")] = matches.group("message")
shifts = {key: shifts[key] for key in sorted(shifts)}
guard_id = 0
guard_sleep_patterns = {}
last_boundry = 0
awake = True
for event_time, event_message in shifts.items():
    if event_message.startswith("Guard"):
        guard_id_match = guard_id_reg.search(event_message)
        guard_id = int(guard_id_match.group("id"))
        if guard_sleep_patterns.get(guard_id, None) is None:
            guard_sleep_patterns[guard_id] = {minute: 0 for minute in range(0, 60)}
        awake = True
        last_boundry = 0
        continue
    next_boundry = int(event_time[14:16])
    for i in range(last_boundry, next_boundry):
        guard_sleep_patterns[guard_id][i] = guard_sleep_patterns[guard_id].get(i, 0) + (0 if awake else 1)
    awake = not awake
    last_boundry = next_boundry
laziest_minute = -1
laziest_guard = 0
best_minute = -1
for guard_id, pattern in guard_sleep_patterns.items():
    for key, value in pattern.items():
        if value > best_minute:
            best_minute = value
            laziest_guard = guard_id
            laziest_minute = key
print(laziest_minute * laziest_guard)
