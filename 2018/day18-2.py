def count_items(string):
    return sum([1 for char in string if char == "|"]), \
           sum([1 for char in string if char == "#"]), \
           sum([1 for char in string if char == "."])


def get_next_char(col, row, string):
    top_string = ""
    bot_string = ""
    left_offset = 0 if col == 0 else col - 1
    right_offset = col if col == width - 1 else col + 1
    if row > 0:
        top_string = string[((row - 1) * width) + left_offset:((row - 1) * width) + right_offset + 1]
    if row < height:
        bot_string = string[((row + 1) * width) + left_offset:((row + 1) * width) + right_offset + 1]
    mid_string = (string[(row * width) + left_offset] if (row * width) + left_offset != (row * width) + col else "") + \
                 (string[(row * width) + right_offset] if (row * width) + right_offset != (row * width) + col else "")
    this_char = string[(row * width) + col]
    full_string = top_string + mid_string + bot_string
    char_counts = count_items(full_string)
    if this_char == ".":
        return "|" if char_counts[0] >= 3 else "."
    if this_char == "|":
        return "#" if char_counts[1] >= 3 else "|"
    if this_char == "#":
        return "#" if char_counts[1] >= 1 and char_counts[0] >= 1 else "."


def print_string(string):
    for row in range(height):
        print(string[row * width:row * width + width])
    print()


scan_raw = [line.strip() for line in open("day18.txt", "r")]
scan_string = "".join(scan_raw)
width = len(scan_raw[0])
height = len(scan_raw)
store = []
for minute in range(1000000000):
    new_string_list = []
    for y in range(height):
        for x in range(width):
            new_string_list.append(get_next_char(x, y, scan_string))

    new_string = "".join(new_string_list)

    if new_string in store:
        ind = (((1000000000 - store.index(new_string)) % (minute - store.index(new_string))) + store.index(
            new_string)) - 1
        scan_string = store[ind]
        break
    scan_string = new_string
    store.append(scan_string)
counts = count_items(scan_string)
print(counts[0] * counts[1])
