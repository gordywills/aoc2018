import re


class Star:
    pos_x = 0
    pos_y = 0
    vel_x = 0
    vel_y = 0

    def __init__(self, pos_x, pos_y, vel_x, vel_y):
        self.pos_x = pos_x
        self.pos_y = pos_y
        self.vel_x = vel_x
        self.vel_y = vel_y

    def step_forward(self, steps):
        self.pos_x = self.pos_x + (self.vel_x * steps)
        self.pos_y = self.pos_y + (self.vel_y * steps)

    def step_backward(self, steps):
        self.pos_x = self.pos_x - (self.vel_x * steps)
        self.pos_y = self.pos_y - (self.vel_y * steps)

    def is_here(self, x, y):
        return x == self.pos_x and y == self.pos_y


def get_area(sky_dict):
    left_limit = min([star.pos_x for k, star in sky_dict.items()])
    right_limit = max([star.pos_x for k, star in sky_dict.items()])
    low_limit = min([star.pos_y for k, star in sky_dict.items()])
    high_limit = max([star.pos_y for k, star in sky_dict.items()])
    width = right_limit - left_limit
    height = high_limit - low_limit
    return width * height


raw_start_states = [line.strip() for line in open("day10.txt", "r")]
reg = re.compile(
    "position=<\s*(?P<start_x>-*\d*),\s*(?P<start_y>-*\d*)> velocity=<\s*(?P<vel_x>-*\d*),\s*(?P<vel_y>-*\d*)>")
sky = {}
x = 0
for state in raw_start_states:
    matches = reg.search(state)
    sky[x] = Star(int(matches.group("start_x")), int(matches.group("start_y")), int(matches.group("vel_x")),
                  int(matches.group("vel_y")))
    x += 1
last_area = get_area(sky)
current_area = last_area - 1
x = 0
while last_area > current_area:
    x = x + 1
    last_area = current_area
    for k, star in sky.items():
        star.step_forward(1)
    current_area = get_area(sky)
print(x - 1)
