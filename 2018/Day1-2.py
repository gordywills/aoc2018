answer = 0
answer_list = {answer: answer}
while True:
    for increment in [int(line.strip()) for line in open("Day1.txt", "r")]:
        answer = answer + increment
        if answer in answer_list:
            print(answer)
            exit()
        answer_list[answer] = answer
