import re

reg = re.compile("#(?P<patch_id>\d*) @ (?P<x_coord>\d*),(?P<y_coord>\d*): (?P<width>\d*)x(?P<height>\d*)")
loc_dict = {}
for patch in [line.strip() for line in open("day3.txt", "r")]:
    matches = reg.search(patch)
    patch_id = matches.group("patch_id")
    x_coord = matches.group("x_coord")
    y_coord = matches.group("y_coord")
    width = matches.group("width")
    height = matches.group("height")
    for xs in range(int(width)):
        for ys in range(int(height)):
            address = str(int(x_coord) + xs) + "-" + str(int(y_coord) + ys)
            loc_dict[address] = loc_dict.get(address, 0) + 1
answer = sum(1 for count in loc_dict.values() if count > 1)
print(answer)
