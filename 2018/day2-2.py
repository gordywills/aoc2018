import re

two_count = 0
three_count = 0
id_list = [line.strip() for line in open("day2.txt", "r")]
while id_list.__len__() > 0:
    box_id_candidate = id_list.pop()
    for x in range(box_id_candidate.__len__()):
        box_id_regex = list(box_id_candidate)
        box_id_regex[x] = "."
        box_id_regex = "".join(box_id_regex)
        reg = re.compile(box_id_regex)
        matches = next((box_id for box_id in id_list if reg.match(box_id)), None)
        if matches:
            answer = "".join([x for x in matches if x in box_id_candidate])
            print(answer)
            exit()
