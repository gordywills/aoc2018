import re


def build_machine(states):
    start_state_reg = re.compile("Begin in state (?P<start_state>.)\.")
    number_of_steps_reg = re.compile("Perform a diagnostic checksum after (?P<number_of_steps>\d*) steps\.")
    instruction_set_reg = re.compile(
        "In state (?P<state>.):\n\s*If the current value is (?P<opt_1>.):\n\s*- Write the value (?P<type_1>.)\.\n\s*- Move one slot to the (?P<act_1>.*)\.\n\s*- Continue with state (?P<next_state_1>.)\.\n\s*If the current value is (?P<opt_2>.):\n\s*- Write the value (?P<type_2>.)\.\n\s*- Move one slot to the (?P<act_2>.*)\.\n\s*- Continue with state (?P<next_state_2>.)\.")
    instruction_sets_raw = instruction_set_reg.findall(states)
    instructions = {}
    for isr in instruction_sets_raw:
        instructions[isr[0]] = {int(isr[1]): {"type": int(isr[2]),
                                              "action": 1 if isr[3] == "right" else -1,
                                              "next_state": isr[4]},
                                int(isr[5]): {"type": int(isr[6]),
                                              "action": 1 if isr[7] == "right" else -1,
                                              "next_state": isr[8]},
                                }
    start_state_matches = start_state_reg.search(states)
    start_state = start_state_matches.group("start_state")
    number_of_steps_matches = number_of_steps_reg.search(states)
    number_of_steps = int(number_of_steps_matches.group("number_of_steps"))
    return instructions, start_state, number_of_steps


def run_machine(machine, current_state, iterations):
    tape = [0]
    pointer = 0
    for x in range(iterations):
        value = tape[pointer]
        tape[pointer] = machine[current_state][value]["type"]
        if pointer == 0 and machine[current_state][value]["action"] == -1:
            tape.insert(0, 0)
        elif pointer == len(tape) - 1 and machine[current_state][value]["action"] == 1:
            tape.insert(len(tape), 0)
            pointer = pointer + machine[current_state][value]["action"]
        else:
            pointer = pointer + machine[current_state][value]["action"]
        current_state = machine[current_state][value]["next_state"]
    return tape


with open("2017day25.txt", "r") as fd:
    raw_states = fd.read()
buffer = run_machine(*build_machine(raw_states))
print(sum(buffer))
