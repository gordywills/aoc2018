def is_pair(x, y):
    if ((x.isupper() and y.islower()) or (y.isupper() and x.islower())) and (x.lower() == y.lower()):
        return True
    return False


with open("day5.txt","r") as fd:
    formula = fd.read()
formula = list(formula)

reduced = []
for x in formula:
    if reduced.__len__() == 0:
        reduced.append(x)
        continue
    if is_pair(reduced[reduced.__len__() - 1], x):
        reduced.pop()
        continue
    else:
        reduced.append(x)
print(reduced.__len__())
exit()



# THIS IS THE SLOW WAY TO DAY THAT

with open("day5.txt","r") as fd:
    formula = fd.read()
formula = {x: formula[x] for x in range(formula.__len__())}

found = True
while found:
    found = False
    for x, y in formula.items():
        if x == formula.__len__()-1:
            continue
        if is_pair(formula[x], formula[x+1]):
            del formula[x]
            del formula[x+1]
            found = True
            break
    formula = dict(enumerate(x for x in formula.values()))
print(formula.__len__())
