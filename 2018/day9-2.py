import re
from collections import deque


def get_input():
    with open("day9.txt", "r") as fd:
        raw_game_params = fd.read()
    reg = re.compile("(?P<players>\d*) players; last marble is worth (?P<max_marble>\d*) points")
    matches = reg.match(raw_game_params)
    return matches.groupdict()


game_params = get_input()
circle = deque([0])
scores = {}
for x in range(1, (int(game_params["max_marble"]) * 100) + 1):
    if (x % 23) == 0:
        circle.rotate(7)
        scores[x % int(game_params["players"])] = scores.get(x % int(game_params["players"]), 0) + circle.pop() + x
        circle.rotate(-1)
    else:
        circle.rotate(-1)
        circle.append(x)
print(max(scores.values()))
