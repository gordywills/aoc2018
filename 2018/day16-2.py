import re


class Machine:
    registers = None

    def __init__(self):
        self.registers = {0: 0, 1: 0, 2: 0, 3: 0}

    def set_register(self, reg: int, value: int):
        self.registers[reg] = value

    def read_register(self, reg: int) -> int:
        return self.registers[reg]

    def read_registers(self) -> [int]:
        return [x for x in self.registers.values()]

    def set_registers(self, regs: [int]):
        for x in range(4):
            self.set_register(x, regs[x])

    def addr(self, a: int, b: int, c: int):
        self.set_register(c, (self.read_register(a) + self.read_register(b)))

    def addi(self, a: int, b: int, c: int):
        self.set_register(c, (self.read_register(a) + b))

    def mulr(self, a: int, b: int, c: int):
        self.set_register(c, (self.read_register(a) * self.read_register(b)))

    def muli(self, a: int, b: int, c: int):
        self.set_register(c, (self.read_register(a) * b))

    def banr(self, a: int, b: int, c: int):
        self.set_register(c, (self.read_register(a) & self.read_register(b)))

    def bani(self, a: int, b: int, c: int):
        self.set_register(c, (self.read_register(a) & b))

    def borr(self, a: int, b: int, c: int):
        self.set_register(c, (self.read_register(a) | self.read_register(b)))

    def bori(self, a: int, b: int, c: int):
        self.set_register(c, (self.read_register(a) | b))

    def setr(self, a: int, b: int, c: int):
        self.set_register(c, (self.read_register(a)))

    def seti(self, a: int, b: int, c: int):
        self.set_register(c, a)

    def gtir(self, a: int, b: int, c: int):
        self.set_register(c, int(a > self.read_register(b)))

    def gtri(self, a: int, b: int, c: int):
        self.set_register(c, int(self.read_register(a) > b))

    def gtrr(self, a: int, b: int, c: int):
        self.set_register(c, int(self.read_register(a) > self.read_register(b)))

    def eqir(self, a: int, b: int, c: int):
        self.set_register(c, int(a == self.read_register(b)))

    def eqri(self, a: int, b: int, c: int):
        self.set_register(c, int(self.read_register(a) == b))

    def eqrr(self, a: int, b: int, c: int):
        self.set_register(c, int(self.read_register(a) == self.read_register(b)))


test_reg = re.compile(".*: \[(\d+), (\d+), (\d+), (\d+)]\n(\d+) (\d+) (\d+) (\d+)\n.*:\s*\[(\d+), (\d+), (\d+), (\d+)]")
with open("day16-1.txt", "r") as fd:
    test_scenarios_raw = fd.read()
test_scenarios_matches = test_reg.findall(test_scenarios_raw)

machine = Machine()
op_code_possible = {x: {} for x in range(16)}
test_count = 0
ops = ["addr", "addi", "mulr", "muli", "banr", "bani", "borr", "bori", "setr", "seti", "gtir", "gtri", "gtrr",
       "eqir", "eqri", "eqrr"]
for scenario in test_scenarios_matches:
    possible_count = 0
    machine.set_registers([int(scenario[0]), int(scenario[1]), int(scenario[2]), int(scenario[3])])
    initial_state = machine.read_registers()
    for op in ops:
        func = getattr(machine, op)
        func(int(scenario[5]), int(scenario[6]), int(scenario[7]))
        new_state = machine.read_registers()
        if new_state == [int(scenario[8]), int(scenario[9]), int(scenario[10]), int(scenario[11])]:
            possible_count += 1
            op_code_possible[int(scenario[4])][op] = True
        machine.set_registers(initial_state)
    if possible_count >= 3:
        test_count += 1

op_codes = {}
while len(op_codes) < 16:
    for code, possible in op_code_possible.items():
        if len(possible) == 1:
            op_codes[code] = list(possible.keys())[0]
            del op_code_possible[code]
            op_code_possible = {c: {key: True for key, value in pos.items() if key != op_codes[code]} for c, pos in
                                op_code_possible.items()}
            break

instructions_raw = [line.strip() for line in open("day16-2.txt", "r")]
instr_reg = re.compile("(?P<op_code>\d+) (?P<A>\d+) (?P<B>\d+) (?P<C>\d+)")
machine.set_registers([0, 0, 0, 0])
func_lookup = {key: getattr(machine, func_name) for key, func_name in op_codes.items()}

for instruction in instructions_raw:
    matches = instr_reg.match(instruction)
    func_lookup[int(matches.group("op_code"))](int(matches.group("A")),
                                               int(matches.group("B")),
                                               int(matches.group("C")))

print(machine.read_registers())
