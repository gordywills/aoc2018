def get_best_grid_power(power_levels_dict):
    max_power = 3 * 3 * -5
    best_loc = None
    for y in range(1, 298):
        for x in range(1, 298):
            resultant_power = 0
            for a in range(3):
                for b in range(3):
                    resultant_power += power_levels_dict[y + a][x + b]
            if resultant_power > max_power:
                max_power = resultant_power
                best_loc = resultant_power, x, y
    return best_loc


serial_number = 2694
power_levels = {
    y: {
        x: int(str(((((x + 10) * y) + serial_number) * (x + 10)))[-3:-2]) - 5 for x in range(1, 301)
    } for y in range(1, 301)
}
print(get_best_grid_power(power_levels))
