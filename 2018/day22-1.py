depth = 7740
target = (12, 763)

cave = {}
risk = 0
for x in range(target[0] + 1):
    cave[x] = {}
    for y in range(target[1] + 1):
        if (x == 0 and y == 0) or (x == target[0] and y == target[1]):
            geo_index = 0
        elif y == 0:
            geo_index = x * 16807
        elif x == 0:
            geo_index = y * 48271
        else:
            geo_index = cave[x - 1][y]["erosion"] * cave[x][y - 1]["erosion"]
        erosion_level = (geo_index + depth) % 20183
        region_type = erosion_level % 3
        risk += region_type
        cave[x][y] = {"geo": geo_index, "erosion": erosion_level, "region_type": region_type}
print(risk)
