two_count = 0
three_count = 0
for box_id in [line.strip() for line in open("day2.txt", "r")]:
    letter_count_dict = {}
    for letter in box_id:
        letter_count_dict[letter] = letter_count_dict.get(letter, 0) + 1
    if 2 in letter_count_dict.values():
        two_count = two_count + 1
    if 3 in letter_count_dict.values():
        three_count = three_count + 1
print(two_count * three_count)
