iterations = "635041"
score_list = "37"
e1p = 0
e2p = 1
while len(score_list) < (int(iterations) + 10):
    score_list += str(int(score_list[e1p]) + int(score_list[e2p]))
    e1p = (e1p + int(score_list[e1p]) + 1) % len(score_list)
    e2p = (e2p + int(score_list[e2p]) + 1) % len(score_list)
answer = score_list[-10:]
print(answer)
