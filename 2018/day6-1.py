import re


def calc_dist(x1,y1,x2,y2):
    return abs(x1 - x2) + abs(y1 -y2)


raw_coords = [line.strip() for line in open("day6.txt", "r")]
reg = re.compile("(?P<x>\d*), (?P<y>\d*)")
index = 0
highest_x = -1
highest_y = -1
coords = {}
for x in raw_coords:
    matches = reg.match(x)
    coords[index] = {"x": int(matches.group("x")), "y": int(matches.group("y"))}
    highest_x = int(matches.group("x")) if int(matches.group("x")) > highest_x else highest_x
    highest_y = int(matches.group("y")) if int(matches.group("y")) > highest_y else highest_y
    index += 1
dist_map = {}
for test_x in range(highest_x):
    dist_map[test_x] = {}
    for test_y in range(highest_y):
        dists = {}
        for key, point in coords.items():
            dists[key] = calc_dist(test_x, test_y, point["x"], point["y"])
        nearest_dist = highest_y + highest_x + 1
        nearest_point = None
        for key, dist in dists.items():
            if dist < nearest_dist :
                nearest_dist = dist
                nearest_point = key
                continue
            if dist == nearest_dist:
                nearest_point = None
        dist_map[test_x][test_y] = nearest_point
node_sizes = {}
for dist_x, ys in dist_map.items():
    for dist_y, node in ys.items():
        if node is None:
            continue
        if dist_x == 0 or dist_x == highest_x or dist_y == 0 or dist_y == highest_y:
            node_sizes[node] = "infinite"
            continue
        if node_sizes.get(node, None) != "infinite":
            node_sizes[node] = node_sizes.get(node, 0) + 1
filtered_node_sizes = {node: size for node, size in node_sizes.items() if size != "infinite"}
biggest_size = -1
biggest_node = None
for node, size in filtered_node_sizes.items():
    if size > biggest_size:
        biggest_size = size
        biggest_node = node
print(biggest_node)
print(biggest_size)
