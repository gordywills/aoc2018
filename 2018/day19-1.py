import re


class Machine:
    registers = None

    def __init__(self):
        self.registers = {0: 0, 1: 0, 2: 0, 3: 0, 4: 0, 5: 0}

    def set_register(self, reg: int, value: int):
        self.registers[reg] = value

    def read_register(self, reg: int) -> int:
        return self.registers[reg]

    def read_registers(self) -> [int]:
        return [x for x in self.registers.values()]

    def set_registers(self, regs: [int]):
        for x in range(4):
            self.set_register(x, regs[x])

    def addr(self, a: int, b: int, c: int):
        self.set_register(c, (self.read_register(a) + self.read_register(b)))

    def addi(self, a: int, b: int, c: int):
        self.set_register(c, (self.read_register(a) + b))

    def mulr(self, a: int, b: int, c: int):
        self.set_register(c, (self.read_register(a) * self.read_register(b)))

    def muli(self, a: int, b: int, c: int):
        self.set_register(c, (self.read_register(a) * b))

    def banr(self, a: int, b: int, c: int):
        self.set_register(c, (self.read_register(a) & self.read_register(b)))

    def bani(self, a: int, b: int, c: int):
        self.set_register(c, (self.read_register(a) & b))

    def borr(self, a: int, b: int, c: int):
        self.set_register(c, (self.read_register(a) | self.read_register(b)))

    def bori(self, a: int, b: int, c: int):
        self.set_register(c, (self.read_register(a) | b))

    def setr(self, a: int, b: int, c: int):
        self.set_register(c, (self.read_register(a)))

    def seti(self, a: int, b: int, c: int):
        self.set_register(c, a)

    def gtir(self, a: int, b: int, c: int):
        self.set_register(c, int(a > self.read_register(b)))

    def gtri(self, a: int, b: int, c: int):
        self.set_register(c, int(self.read_register(a) > b))

    def gtrr(self, a: int, b: int, c: int):
        self.set_register(c, int(self.read_register(a) > self.read_register(b)))

    def eqir(self, a: int, b: int, c: int):
        self.set_register(c, int(a == self.read_register(b)))

    def eqri(self, a: int, b: int, c: int):
        self.set_register(c, int(self.read_register(a) == b))

    def eqrr(self, a: int, b: int, c: int):
        self.set_register(c, int(self.read_register(a) == self.read_register(b)))


instructions_raw = [line.strip() for line in open("day19.txt", "r")]
instr_reg = re.compile("(?P<op_code>.{4}) (?P<A>\d+) (?P<B>\d+) (?P<C>\d+)")
pointer_register = 0
instructions = []
for instr in instructions_raw:
    if instr.startswith("#ip "):
        pointer_register = int(instr[4:])
        continue
    matches = instr_reg.match(instr)
    instructions.append((
        matches.group("op_code"),
        int(matches.group("A")),
        int(matches.group("B")),
        int(matches.group("C"))
    ))

machine = Machine()
pointer = machine.read_register(pointer_register)
while (pointer >= 0) and (pointer < len(instructions)):
    operation = getattr(machine, instructions[pointer][0])
    operation(instructions[pointer][1], instructions[pointer][2], instructions[pointer][3])
    pointer = machine.read_register(pointer_register)
    pointer = pointer + 1
    machine.set_register(pointer_register, pointer)
print(machine.read_registers())
