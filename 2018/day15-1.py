ids = 0


class Unit:
    race = None
    x_pos = None
    y_pos = None
    hit_points = 200
    id = None
    attack_power = 3
    alive = True

    def __init__(self, race, x, y):
        global ids
        self.race = race
        self.x_pos = x
        self.y_pos = y
        ids = ids + 1
        self.id = ids

    def make_a_move(self, unit_list, cave_map):
        target_list = []
        for unit in unit_list:
            if unit.race == self.race or unit.is_alive() is not True:
                continue
            if unit.is_alive() and \
                    (unit.get_row() + 1 == self.get_row() and unit.get_col() == self.get_col()
                     or unit.get_row() == self.get_row() and unit.get_col() + 1 == self.get_col()
                     or unit.get_row() == self.get_row() and unit.get_col() - 1 == self.get_col()
                     or unit.get_row() - 1 == self.get_row() and unit.get_col() == self.get_col()):
                return
            unit_location = unit.get_location()
            self._check_target(cave_map, target_list, unit_list, (unit_location[0], unit_location[1] - 1))
            self._check_target(cave_map, target_list, unit_list, (unit_location[0] - 1, unit_location[1]))
            self._check_target(cave_map, target_list, unit_list, (unit_location[0] + 1, unit_location[1]))
            self._check_target(cave_map, target_list, unit_list, (unit_location[0], unit_location[1] + 1))
        routes = []
        if self.get_location() in target_list:
            return
        for target in target_list:
            self._get_routes(self.get_location(), target, cave_map, unit_list,
                             [self.get_location()], routes)
        if len(routes) > 0:
            next_step = self._process_routes(routes)
            self.x_pos = next_step[0]
            self.y_pos = next_step[1]

    def _process_routes(self, target_routes):
        shortest = None
        for route in target_routes:
            if shortest is None or len(route[1]) < shortest:
                shortest = len(route[1])
        temp_routes = [r for r in target_routes if len(r[1]) == shortest]
        temp_routes = sorted(temp_routes, key=lambda x: (x[0][1], x[0][0]))
        heading = temp_routes[0][0]
        temp_routes = [r for r in temp_routes if r[0] == heading]
        possible_steps = []
        for tr in temp_routes:
            possible_steps.append(tr[1][1])
        possible_steps = sorted(possible_steps, key=lambda x: (x[1], x[0]))
        return possible_steps[0]

    def _get_routes(self, start_loc, target, cave_map, unit_list, visited, target_routes):
        if start_loc == target:
            target_routes.append((target, visited))
            return True

        if (start_loc[0], start_loc[1] - 1) not in [u.get_location() for u in unit_list if u.is_alive() is True] \
                and cave_map[start_loc[1] - 1][start_loc[0]] != "#" \
                and (start_loc[0], start_loc[1] - 1) not in visited:
            self._get_routes((start_loc[0], start_loc[1] - 1), target, cave_map, unit_list,
                             visited + [(start_loc[0], start_loc[1] - 1)], target_routes)

        if (start_loc[0] - 1, start_loc[1]) not in [u.get_location() for u in unit_list if u.is_alive() is True] \
                and cave_map[start_loc[1]][start_loc[0] - 1] != "#" \
                and (start_loc[0] - 1, start_loc[1]) not in visited:
            self._get_routes((start_loc[0] - 1, start_loc[1]), target, cave_map, unit_list,
                             visited + [(start_loc[0] - 1, start_loc[1])], target_routes)

        if (start_loc[0] + 1, start_loc[1]) not in [u.get_location() for u in unit_list if u.is_alive() is True] \
                and cave_map[start_loc[1]][start_loc[0] + 1] != "#" \
                and (start_loc[0] + 1, start_loc[1]) not in visited:
            self._get_routes((start_loc[0] + 1, start_loc[1]), target, cave_map, unit_list,
                             visited + [(start_loc[0] + 1, start_loc[1])], target_routes)

        if (start_loc[0], start_loc[1] + 1) not in [u.get_location() for u in unit_list if u.is_alive() is True] \
                and cave_map[start_loc[1] + 1][start_loc[0]] != "#" \
                and (start_loc[0], start_loc[1] + 1) not in visited:
            self._get_routes((start_loc[0], start_loc[1] + 1), target, cave_map, unit_list,
                             visited + [(start_loc[0], start_loc[1] + 1)], target_routes)

    def _check_target(self, cave_map, target_list, unit_list, target_location):
        if (target_location[0], target_location[1]) not in target_list \
                and cave_map[target_location[1]][target_location[0]] != '#' \
                and (target_location[0], target_location[1]) not in [u.get_location() for u in unit_list if
                                                                     u.is_alive() is True]:
            target_list.append((target_location[0], target_location[1]))

    def make_attack(self, unit_list):
        top_target = None
        left_target = None
        right_target = None
        bottom_target = None
        lowest_life = 201
        for unit in unit_list:
            if unit.race == self.race:
                continue
            if unit.is_alive() and unit.get_row() + 1 == self.get_row() and unit.get_col() == self.get_col():
                top_target = unit
                if top_target.get_hit_point() < lowest_life:
                    lowest_life = top_target.get_hit_point()
            if unit.is_alive() and unit.get_row() == self.get_row() and unit.get_col() + 1 == self.get_col():
                left_target = unit
                if left_target.get_hit_point() < lowest_life:
                    lowest_life = left_target.get_hit_point()
            if unit.is_alive() and unit.get_row() == self.get_row() and unit.get_col() - 1 == self.get_col():
                right_target = unit
                if right_target.get_hit_point() < lowest_life:
                    lowest_life = right_target.get_hit_point()
            if unit.is_alive() and unit.get_row() - 1 == self.get_row() and unit.get_col() == self.get_col():
                bottom_target = unit
                if bottom_target.get_hit_point() < lowest_life:
                    lowest_life = bottom_target.get_hit_point()
        if top_target is not None and top_target.get_hit_point() == lowest_life:
            top_target.set_hit_point(top_target.get_hit_point() - self.attack_power)
            if top_target.get_hit_point() <= 0:
                top_target.make_dead()
        elif left_target is not None and left_target.get_hit_point() == lowest_life:
            left_target.set_hit_point(left_target.get_hit_point() - self.attack_power)
            if left_target.get_hit_point() <= 0:
                left_target.make_dead()
        elif right_target is not None and right_target.get_hit_point() == lowest_life:
            right_target.set_hit_point(right_target.get_hit_point() - self.attack_power)
            if right_target.get_hit_point() <= 0:
                right_target.make_dead()
        elif bottom_target is not None and bottom_target.get_hit_point() == lowest_life:
            bottom_target.set_hit_point(bottom_target.get_hit_point() - self.attack_power)
            if bottom_target.get_hit_point() <= 0:
                bottom_target.make_dead()

    def take_turn(self, unit_list, cave_map):
        self.make_a_move(unit_list, cave_map)
        self.make_attack(unit_list)

    def is_goblin(self) -> bool:
        return self.race == "G"

    def is_elf(self) -> bool:
        return self.race == "E"

    def get_location(self) -> (int, int):
        return self.x_pos, self.y_pos

    def get_col(self) -> int:
        return self.x_pos

    def get_row(self) -> int:
        return self.y_pos

    def get_hit_point(self) -> int:
        return self.hit_points

    def set_hit_point(self, value):
        self.hit_points = value

    def is_alive(self):
        return self.alive

    def make_dead(self):
        self.alive = False

    def __repr__(self):
        return "(" + self.race + str(self.id) + ", " + str(self.hit_points) + ", " + str(self.x_pos) + ", " + str(
            self.y_pos) + ")"


def make_unit_and_map():
    cave_map = {}
    units = []
    map_raw = [line.strip() for line in open("day15.txt", "r")]
    for y in range(len(map_raw)):
        cave_map[y] = {}
        for x in range(len(map_raw[y])):
            if map_raw[y][x] == "#":
                cave_map[y][x] = "#"
            elif map_raw[y][x] == "G" or map_raw[y][x] == "E":
                cave_map[y][x] = "."
                units.append(Unit(map_raw[y][x], x, y))
            else:
                cave_map[y][x] = map_raw[y][x]
    return cave_map, units


caves, fighters = make_unit_and_map()
goblin_count = sum([1 for fighter in fighters if fighter.is_goblin()])
elf_count = sum([1 for fighter in fighters if fighter.is_elf()])
fighters = sorted(fighters, key=lambda x: (x.get_row(), x.get_col()))
round_count = 0
while goblin_count > 0 and elf_count > 0:
    partial = False
    for fighter in fighters:
        if fighter.is_alive():
            fighter.take_turn(fighters, caves)
        for f in fighters:
            if f.get_hit_point() <= 0:
                f.make_dead()
        goblin_count = sum([1 for fighter in fighters if fighter.is_goblin() if fighter.is_alive()])
        elf_count = sum([1 for fighter in fighters if fighter.is_elf() if fighter.is_alive()])
        if (goblin_count == 0 or elf_count == 0) and fighter != fighters[-1]:
            partial = True
            break
    fighters = sorted(fighters, key=lambda x: (x.get_row(), x.get_col()))
    if partial is not True:
        round_count = round_count + 1
    print(round_count)
life_force = sum([x.get_hit_point() for x in fighters if x.is_alive()])
print(round_count)
print(life_force)
print(round_count * life_force)
