import re


def run_rule(minus_two, minus_one, here, plus_one, plus_two):
    for this_rule in rules:
        if minus_two == this_rule[0][0] and minus_one == this_rule[0][1] and here == this_rule[0][2] and plus_one == \
                this_rule[0][3] and plus_two == this_rule[0][4]:
            return this_rule[1]
    return 0


def advance_state(old_state):
    new_state = {}
    low_index = min(old_state)
    new_state[low_index - 2] = run_rule(0, 0, 0, 0, old_state[min(old_state)])
    new_state[low_index - 1] = run_rule(0, 0, 0, old_state[min(old_state)], old_state[min(old_state) + 1])
    for index in old_state:
        state_minus_two = 0 if index - 2 not in old_state else old_state[index - 2]
        state_minus_one = 0 if index - 1 not in old_state else old_state[index - 1]
        current = old_state[index]
        state_plus_one = 0 if index + 1 not in old_state else old_state[index + 1]
        state_plus_two = 0 if index + 2 not in old_state else old_state[index + 2]
        new_state[index] = run_rule(state_minus_two, state_minus_one, current, state_plus_one, state_plus_two)
    high_index = max(old_state)
    new_state[high_index + 1] = run_rule(old_state[max(old_state) - 1], old_state[max(old_state) - 1], 0, 0, 0)
    new_state[high_index + 2] = run_rule(old_state[max(old_state)], 0, 0, 0, 0)
    temp_old_state = "".join([str(z) for z in old_state.values()]).lstrip("0").rstrip("0")
    temp_new_state = "".join([str(z) for z in new_state.values()]).lstrip("0").rstrip("0")
    return new_state, temp_old_state == temp_new_state


max_gens = 50000000000
machine_raw = [line.strip() for line in open("day12.txt", "r")]
state_reg = re.compile("initial state: (?P<state>[.,#]*)")
matches = state_reg.match(machine_raw[0])
state = {counter: 1 if char == '#' else 0 for counter, char in enumerate(matches.group("state"))}
rules = []
rules_reg = re.compile("(?P<rule>[#,.]{5}) => (?P<outcome>[#,.])")
for x in range(2, len(machine_raw)):
    matches = rules_reg.match(machine_raw[x])
    rule = [1 if char == '#' else 0 for char in matches.group("rule")]
    rules.append((rule, 1 if matches.group("outcome") == '#' else 0))
y = 0
for x in range(max_gens):
    state, diff = advance_state(state)
    y += 1
    if diff:
        break
print(sum([(x + max_gens - y) for x in state.keys() if state[x] == 1]))
