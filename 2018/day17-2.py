import re

coord_reg = re.compile(
    "(?P<axis1>[x,y])=(?P<axis1_val>\d+), (?P<axis2>[x,y])=(?P<axis2_val1>\d+)\.\.(?P<axis2_val2>\d+)")
coords_raw = [line.strip() for line in open("day17.txt", "r")]
max_x = 0
min_x = 10000000000000000000
max_y = 0
min_y = 10000000000000000000
coords_tuple_list = []
for encoded_coords in coords_raw:
    matches = coord_reg.match(encoded_coords)
    if matches.group("axis1") == "x" and int(matches.group("axis1_val")) > max_x:
        max_x = int(matches.group("axis1_val"))
    if matches.group("axis1") == "x" and int(matches.group("axis1_val")) < min_x:
        min_x = int(matches.group("axis1_val"))
    if matches.group("axis1") == "y" and int(matches.group("axis1_val")) > max_y:
        max_y = int(matches.group("axis1_val"))
    if matches.group("axis1") == "y" and int(matches.group("axis1_val")) < min_y:
        min_y = int(matches.group("axis1_val"))
    if matches.group("axis2") == "x" and max(int(matches.group("axis2_val1")),
                                             int(matches.group("axis2_val2"))) > max_x:
        max_x = max(int(matches.group("axis2_val1")), int(matches.group("axis2_val2")))
    if matches.group("axis2") == "x" and min(int(matches.group("axis2_val1")),
                                             int(matches.group("axis2_val2"))) < min_x:
        min_x = min(int(matches.group("axis2_val1")), int(matches.group("axis2_val2")))
    if matches.group("axis2") == "y" and max(int(matches.group("axis2_val1")),
                                             int(matches.group("axis2_val2"))) > max_y:
        max_y = max(int(matches.group("axis2_val1")), int(matches.group("axis2_val2")))
    if matches.group("axis2") == "y" and min(int(matches.group("axis2_val1")),
                                             int(matches.group("axis2_val2"))) < min_y:
        min_y = min(int(matches.group("axis2_val1")), int(matches.group("axis2_val2")))
    coords_tuple_list = coords_tuple_list + [(matches.group("axis1"), int(matches.group("axis1_val")),
                                              matches.group("axis2"), int(matches.group("axis2_val1")),
                                              int(matches.group("axis2_val2")))]
scan = {y: {x: "." for x in range(min_x - 2, max_x + 2)} for y in range(0, max_y + 1)}
for coords_tuple in coords_tuple_list:
    if coords_tuple[0] == "x":
        x = coords_tuple[1]
        for y in range(coords_tuple[3], coords_tuple[4] + 1):
            scan[y][x] = "#"
    if coords_tuple[0] == "y":
        y = coords_tuple[1]
        for x in range(coords_tuple[3], coords_tuple[4] + 1):
            scan[y][x] = "#"
scan[0][500] = "+"

row = 1
last_row = 0
while row < len(scan):
    start_row = row
    fill = False
    if row > last_row:
        last_row = row
        for x in range(min_x - 2, max_x + 2):
            if ((scan[row][x] == ".") and (scan[row - 1][x] == "+" or scan[row - 1][x] == "|")):
                scan[row][x] = "|"
                if row == len(scan) - 1:
                    continue
                if scan[row + 1][x] == "#" or scan[row + 1][x] == "~":
                    left_wall_found = False
                    right_wall_found = False
                    for left in range(x, min_x - 2, -1):
                        scan[row][left] = "|"
                        if scan[row + 1][left] == "." or scan[row + 1][left] == "|":
                            break
                        if scan[row][left - 1] == "#":
                            left_wall_found = True
                            left_bound = left
                            break
                    for right in range(x, max_x + 2):
                        scan[row][right] = "|"
                        if scan[row + 1][right] == "." or scan[row + 1][right] == "|":
                            break
                        if scan[row][right + 1] == "#":
                            right_bound = right
                            right_wall_found = True
                            break
                    if left_wall_found and right_wall_found:
                        for z in range(left_bound, right_bound + 1):
                            if scan[row][z] == "|":
                                scan[row][z] = "~"
                        fill = True
    elif row <= last_row:
        last_row = row
        for x in range(min_x - 2, max_x + 2):
            if scan[row][x] == "|" and (scan[row + 1][x] == "#" or scan[row + 1][x] == "~"):
                if row == len(scan) - 1:
                    continue
                left_wall_found = False
                right_wall_found = False
                for left in range(x, min_x - 2, -1):
                    scan[row][left] = "|"
                    if scan[row + 1][left] == "." or scan[row + 1][left] == "|":
                        break
                    if scan[row][left - 1] == "#":
                        left_wall_found = True
                        left_bound = left
                        break
                for right in range(x, max_x + 2):
                    scan[row][right] = "|"
                    if scan[row + 1][right] == "." or scan[row + 1][right] == "|":
                        break
                    if scan[row][right + 1] == "#":
                        right_bound = right
                        right_wall_found = True
                        break
                if left_wall_found and right_wall_found:
                    for z in range(left_bound, right_bound + 1):
                        if scan[row][z] == "|":
                            scan[row][z] = "~"
                    fill = True
    if fill:
        fill = False
        row = row - 1
    if row < last_row:
        continue
    row = row + 1
total = 0
for x in range(min_x - 2, max_x + 2):
    for y in range(min_y, max_y + 1):
        if scan[y][x] == "~":
            total += 1

print(total)
