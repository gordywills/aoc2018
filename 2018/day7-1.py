import re

reg = re.compile("Step (?P<parent>.) .* step (?P<child>.) ")
raw_step = [line.strip() for line in open("day7.txt", "r")]
steps = {}
letters = set()
for instruction in raw_step:
    matches = reg.match(instruction)
    temp = steps.get(matches.group("child"),[])
    temp.append(matches.group("parent"))
    steps[matches.group("child")] = temp
    letters.add(matches.group("child"))
    letters.add(matches.group("parent"))
for letter in letters - set(steps.keys()):
    steps[letter] = []
steps = {key: steps[key] for key in sorted(steps)}
step_order = ""
while len(steps) > 0:
    next_step = ([key for key, value in steps.items() if value == []])
    step_order = step_order + next_step[0]
    del steps[next_step[0]]
    for key, value in steps.items():
        if next_step[0] in value:
            value.remove(next_step[0])
print(step_order)
