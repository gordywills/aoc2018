import functools


@functools.lru_cache(maxsize=None)
def calculate_square(x, y, size):
    if size == 1:
        return power_levels[y][x]
    total_power = calculate_square(x, y, size - 1)
    extra_col_power = 0
    extra_row_power = 0
    for a in range(size):
        extra_col_power += power_levels[y+size-1][x + a]
        extra_row_power += power_levels[y + a][x + size-1]
    total_power += extra_row_power + extra_col_power - (power_levels[y + size-1][x + size-1])
    return total_power


def get_best_square(x, y):
    biggest_value = (300 * 300 * -5) - 1
    best_size = 0
    for size in reversed(range(1, min([300 - x + 1, 300 - y + 1]))):
        if size * size * 5 > biggest_value:
            size_value = calculate_square(x, y, size)
            if size_value > biggest_value:
                biggest_value = size_value
                best_size = size
    return biggest_value, best_size


def get_best_grid_power():
    best_loc = None
    grid_power = {}
    max_power = 0
    for y in reversed(range(1, 301)):
        grid_power[y] = {}
        for x in reversed(range(1, 301)):
            best_local_loc = get_best_square(x, y)
            if best_local_loc[0] > max_power:
                max_power = best_local_loc[0]
                best_loc = best_local_loc[0], x, y, best_local_loc[1]
    return best_loc


serial_number = 2694
power_levels = {
    y: {
        x: int(str(((((x + 10) * y) + serial_number) * (x + 10)))[-3:-2]) - 5 for x in range(1, 301)
    } for y in range(1, 301)
}

print(get_best_grid_power())
