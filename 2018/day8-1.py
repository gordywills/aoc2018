def build_calculate_tree(name, number_list):
    this_node_children_count = number_list.pop(0)
    this_node_metadata_count = number_list.pop(0)
    this_node_children = {}
    total_metadata = 0
    if this_node_children_count:
        for x in range(this_node_children_count):
            next_node = build_calculate_tree(name + x, number_list)
            this_node_children.update(next_node[0])
            total_metadata = total_metadata + next_node[1]
    this_node_metadata = []
    for x in range(this_node_metadata_count):
        metadata = number_list.pop(0)
        this_node_metadata.append(metadata)
        total_metadata = total_metadata + metadata
    return (
        {name: (this_node_children_count, this_node_metadata_count, this_node_children, this_node_metadata)},
        total_metadata
    )


with open("day8.txt", "r") as fd:
    raw_numbers = fd.read()
numbers = [int(x) for x in raw_numbers.split()]
tree = build_calculate_tree(0, numbers)
print(tree[1])
