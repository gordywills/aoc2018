class Cart:
    LEFT = 0
    STRAIGHT = 1
    RIGHT = 2
    direction = None
    x_pos = None
    y_pos = None
    next_turn = LEFT

    def __init__(self, direction, x, y):
        self.direction = direction
        self.x_pos = x
        self.y_pos = y

    def make_step(self, routes):
        if self.direction == "^":
            self.y_pos = self.y_pos - 1
        if self.direction == "v":
            self.y_pos = self.y_pos + 1
        if self.direction == "<":
            self.x_pos = self.x_pos - 1
        if self.direction == ">":
            self.x_pos = self.x_pos + 1
        new_pos = routes[self.y_pos][self.x_pos]
        if new_pos is None:
            raise ValueError
        if new_pos == "+":
            self._handle_junction()
        if new_pos == "/":
            self._handle_corner_fwd()
        if new_pos == "\\":
            self._handle_corner_back()

    def _handle_corner_back(self):
        if self.direction == "^":
            self.direction = "<"
        elif self.direction == "v":
            self.direction = ">"
        elif self.direction == "<":
            self.direction = "^"
        elif self.direction == ">":
            self.direction = "v"

    def _handle_corner_fwd(self):
        if self.direction == "^":
            self.direction = ">"
        elif self.direction == "v":
            self.direction = "<"
        elif self.direction == "<":
            self.direction = "v"
        elif self.direction == ">":
            self.direction = "^"

    def _handle_junction(self):
        if (self.direction == "^" and self.next_turn == self.LEFT) or (self.direction == "v" and self.next_turn == self.RIGHT):
            self.direction = "<"
        elif (self.direction == "^" and self.next_turn == self.RIGHT) or (self.direction == "v" and self.next_turn == self.LEFT):
            self.direction = ">"
        elif (self.direction == "<" and self.next_turn == self.LEFT) or (self.direction == ">" and self.next_turn == self.RIGHT):
            self.direction = "v"
        elif (self.direction == "<" and self.next_turn == self.RIGHT) or (self.direction == ">" and self.next_turn == self.LEFT):
            self.direction = "^"
        self.next_turn = (self.next_turn + 1) % 3

    def get_location(self) -> (int, int):
        return self.x_pos, self.y_pos

    def get_col(self) -> int:
        return self.x_pos

    def get_row(self) -> int:
        return self.y_pos

    def __repr__(self):
        return "(" + self.direction + ", " + str(self.x_pos) + ", " + str(self.y_pos) + ")"


def detect_collision(cart_list) -> (bool, (int, int)):
    for k, v in enumerate(cart_list):
        for x in range(k + 1, len(cart_list)):
            if v.get_row() == cart_list[x].get_row() and v.get_col() == cart_list[x].get_col():
                return True, (v.get_location())
    return False, (0, 0)


def make_rails_and_carts():
    rails = {}
    carts = []
    rails_raw = [line for line in open("day13.txt", "r")]
    for y in range(len(rails_raw)):
        rails[y] = {}
        for x in range(len(rails_raw[y])):
            if rails_raw[y][x] == " ":
                rails[y][x] = " "
            elif rails_raw[y][x] == ">" or rails_raw[y][x] == "<":
                rails[y][x] = "-"
                carts.append(Cart(rails_raw[y][x], x, y))
            elif rails_raw[y][x] == "v" or rails_raw[y][x] == "^":
                rails[y][x] = "|"
                carts.append(Cart(rails_raw[y][x], x, y))
            else:
                rails[y][x] = rails_raw[y][x]
    return rails, carts


rails, carts = make_rails_and_carts()
carts = sorted(carts, key=lambda x: (x.get_row(), x.get_col()))
while not detect_collision(carts)[0]:
    for cart in carts:
        cart.make_step(rails)
        if detect_collision(carts)[0]:
            break
    carts = sorted(carts, key=lambda x: (x.get_row(), x.get_col()))
print(detect_collision(carts))
