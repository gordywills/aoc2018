import re


def calc_dist(x1,y1,x2,y2):
    return abs(x1 - x2) + abs(y1 -y2)


raw_coords = [line.strip() for line in open("day6.txt", "r")]
reg = re.compile("(?P<x>\d*), (?P<y>\d*)")
index = 0
highest_x = -1
highest_y = -1
coords = {}
space_count = 0
for x in raw_coords:
    matches = reg.match(x)
    coords[index] = {"x": int(matches.group("x")), "y": int(matches.group("y"))}
    highest_x = int(matches.group("x")) if int(matches.group("x")) > highest_x else highest_x
    highest_y = int(matches.group("y")) if int(matches.group("y")) > highest_y else highest_y
    index += 1
dist_map = {}
for test_x in range(highest_x):
    dist_map[test_x] = {}
    for test_y in range(highest_y):
        dists = {}
        for key, point in coords.items():
            dists[key] = calc_dist(test_x, test_y, point["x"], point["y"])
        total_dist = sum(dists.values())
        if total_dist < 10000:
            space_count += 1
print(space_count)
