import re

reg = re.compile("#(?P<patch_id>\d*) @ (?P<x_coord>\d*),(?P<y_coord>\d*): (?P<width>\d*)x(?P<height>\d*)")
loc_dict = {}
patches = [line.strip() for line in open("day3.txt", "r")]
patches_translated = {}
for patch in patches:
    matches = reg.search(patch)
    patch_id = matches.group("patch_id")
    x_coord = matches.group("x_coord")
    y_coord = matches.group("y_coord")
    width = matches.group("width")
    height = matches.group("height")
    patches_translated[patch_id] = {"x": x_coord, "y": y_coord, "width": width, "height": height}
    for xs in range(int(width)):
        for ys in range(int(height)):
            address = str(int(x_coord) + xs) + "-" + str(int(y_coord) + ys)
            data = loc_dict.get(address, None)
            if data:
                loc_dict[address][0] = loc_dict[address][0] + 1
                loc_dict[address][1] = patch_id
            else:
                loc_dict[address] = [1, patch_id]

potentials = {address: data[1] for address, data in loc_dict.items() if data[0] == 1}
for potential_id in list(set(potentials.values())):
    patch_addresses = []
    for xs in range(int(patches_translated[potential_id]["width"])):
        for ys in range(int(patches_translated[potential_id]["height"])):
            patch_addresses.append(str(int(patches_translated[potential_id]["x"]) + xs) + "-" + str(int(patches_translated[potential_id]["y"]) + ys))
    if set(patch_addresses) < set(potentials.keys()):
        answer = potential_id
        print(answer)
        exit()