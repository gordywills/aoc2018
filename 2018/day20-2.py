with open("day20.txt", "r") as fd:
    regex_path = fd.read()
movement = {"N": (0, -1), "E": (1, 0), "S": (0, 1), "W": (-1, 0)}
decision_point = []
x, y = 0, 0
last_x, last_y = x, y
distances = {}
dist = 0
for char in regex_path[1:-1]:
    if char == "(":
        decision_point.append((x, y))
    elif char == ")":
        x, y = decision_point.pop()
    elif char == "|":
        x, y = decision_point[-1]
    else:
        move_x, move_y = movement[char]
        x += move_x
        y += move_y
        if distances.get((x, y), None) is not None:
            distances[(x, y)] = min(distances[(x, y)], distances[(last_x, last_y)] + 1)
        else:
            distances[(x, y)] = distances.get((last_x, last_y), 0) + 1
    last_x, last_y = x, y
print(len([x for x in distances.values() if x >= 1000]))
