def is_pair(x, y):
    return ((x.isupper() and y.islower()) or (y.isupper() and x.islower())) and (x.lower() == y.lower())


def char_range(c1, c2):
    for c in range(ord(c1), ord(c2) + 1):
        yield chr(c)


with open("day5.txt","r") as fd:
    formula = fd.read()
formula = list(formula)

reduced = []
for x in formula:
    if reduced.__len__() == 0:
        reduced.append(x)
        continue
    if is_pair(reduced[reduced.__len__() - 1], x):
        reduced.pop()
        continue
    else:
        reduced.append(x)
formula = reduced
shortest = formula.__len__() + 1
for char in char_range("a", "z"):
    reduced = []
    for x in formula:
        if x.lower() == char:
            continue
        if reduced.__len__() == 0:
            reduced.append(x)
            continue
        if is_pair(reduced[reduced.__len__() - 1], x):
            reduced.pop()
            continue
        else:
            reduced.append(x)
    if reduced.__len__() < shortest:
        shortest = reduced.__len__()
print(shortest)
