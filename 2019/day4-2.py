lower_bound = 193651
upper_bound = 649729


def double_chars(number):
    count = {}
    chars = list(str(number))
    for x in range(0, len(chars) - 1):
        if chars[x] in count.keys():
            continue
        count[chars[x]] = 0
        for y in range(x, len(chars) - 1):
            if chars[x] == chars[y]:
                count[chars[x]] += 1
    if chars[-1] in count.keys():
        count[chars[-1]] += 1
    else:
        count[chars[-1]] = 1
    if 2 in count.values():
        return True
    return False


def increasing_digits(number):
    chars = list(str(number))
    for x in range(0, len(chars) - 1):
        if int(chars[x]) > int(chars[x + 1]):
            return False
    return True


candidates = []
for candidate in range(lower_bound, upper_bound + 1):
    if double_chars(candidate) and increasing_digits(candidate):
        candidates.append(candidate)

print(len(candidates))
