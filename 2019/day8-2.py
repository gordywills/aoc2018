with open("day8.txt", "r") as fd:
    pixels = fd.readline()

height = 6
width = 25
page_size = height * width
layer_count = int(len(pixels) / page_size)
layer_map = {}
for layer in range(layer_count):
    layer_map[layer] = pixels[layer * page_size:((layer + 1) * page_size)]

visible=[]
for pixel in range(page_size):
    layer_counter=0
    while layer_map[layer_counter][pixel] == '2':
        layer_counter+=1
    if layer_map[layer_counter][pixel] == '0':
        vis_char=' '
    elif layer_map[layer_counter][pixel] == '1':
        vis_char='$'
    else:
        vis_char='?'
    visible.append(vis_char)


for line in range(height):
    print("".join(visible[width*line:((line+1)*width)]))
