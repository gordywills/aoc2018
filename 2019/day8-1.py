with open("day8.txt", "r") as fd:
    pixels = fd.readline()


def char_count(val, needle):
    local_char_count = 0
    for char in val:
        if char == needle:
            local_char_count += 1
    return local_char_count


height = 6
width = 25
page_size = height * width
layer_count = int(len(pixels) / (page_size))
layer_map = {}
for layer in range(0, layer_count):
    layer_map[layer] = pixels[layer * page_size:((layer + 1) * page_size)]

zero_count = -1
target = -1

for key, val in layer_map.items():
    local_zero_count = char_count(val, '0')
    if zero_count == -1 or local_zero_count < zero_count:
        zero_count = local_zero_count
        target = key

ones = char_count(layer_map[target], '1')
twos = char_count(layer_map[target], '2')
print(ones * twos)
