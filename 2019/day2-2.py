with open("day2.txt", "r") as fd:
    init_opcodes = fd.read()
init_opcodes = init_opcodes.split(",")
init_opcodes = [int(x) for x in init_opcodes]

for noun in range(100):
    for verb in range(100):
        opcodes = init_opcodes.copy()
        position = 0
        opcodes[1] = noun
        opcodes[2] = verb
        while opcodes[position] != 99:
            if opcodes[position] == 1:
                opcodes[opcodes[position + 3]] = opcodes[opcodes[position + 1]] + opcodes[opcodes[position + 2]]
            if opcodes[position] == 2:
                opcodes[opcodes[position + 3]] = opcodes[opcodes[position + 1]] * opcodes[opcodes[position + 2]]
            position = position + 4
        if opcodes[0] == 19690720:
            print(noun * 100 + verb)
            exit()
