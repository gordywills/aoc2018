import anytree

with open("day6.txt", "r") as fd:
    lines = fd.readlines()
raw_orbits = {}
all_objects = {}
for line in lines:
    if line[:3] not in raw_orbits.keys():
        raw_orbits[line[:3]] = [line[4:].strip()]
    else:
        raw_orbits[line[:3]].append(line[4:].strip())
    if line[:3] not in all_objects.keys():
        all_objects[line[:3]] = None
    if line[4:].strip() not in all_objects.keys():
        all_objects[line[4:].strip()] = None


def build_tree(parent, name, raw_orbits):
    global all_objects
    node = anytree.Node(name, parent=parent)
    all_objects[name] = node
    if name in raw_orbits.keys():
        for child in raw_orbits[name]:
            build_tree(node, child, raw_orbits)
    return node


orbit_tree = build_tree(None, "COM", raw_orbits)

you_to_root = [str(node.name) for node in all_objects["YOU"].path]
san_to_root = [str(node.name) for node in all_objects["SAN"].path]
while True:
    if you_to_root[1] == san_to_root[1]:
        del you_to_root[0]
        del san_to_root[0]
    else:
        break

path = list(reversed(you_to_root[1:-1])) + san_to_root[:-1]
print(len(path) - 1)
