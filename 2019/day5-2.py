with open("day5.txt", "r") as fd:
    program = fd.read()
program = program.split(",")
program = [int(x) for x in program]


class Machine:
    program = []
    program_position = 0
    input_data = []
    input_position = 0

    def __init__(self, program, input_data, program_position=0, input_position=0):
        self.program = program
        self.input_data = input_data
        self.program_position = program_position
        self.input_position = input_position

    def run_program(self):
        while self.program[self.program_position] != 99:
            position = self.program[self.program_position]
            opcode = self.read_opcode(position)
            props = self.get_opcode_props(opcode)
            param_modes = self.get_param_modes(position)
            param_modes = self.pad_param_modes(param_modes, props['param_count'])
            params = self._get_params(param_modes, props['write_params'])
            props['function'](params)
            self.program_position = self.program_position + props['next_instr_step']

    def add_code(self, params):
        self.program[params[2]] = params[0] + params[1]

    def _get_params(self, param_modes, write_params):
        params = []
        for i, mode in enumerate(param_modes):
            if (mode == 1) or (i in write_params):
                params.append(self.program[self.program_position + i + 1])
            else:
                params.append(self.program[self.program[self.program_position + i + 1]])

        return params

    def mult_code(self, params):
        self.program[params[2]] = params[0] * params[1]

    def input_code(self, params):
        self.program[params[0]] = self.input_data[self.input_position]
        self.input_position+=1

    def output_code(self, params):
        print(self.program[params[0]])

    def true_skip_code(self, params):
        if params[0] != 0:
            self.program_position=params[1]
        else:
            self.program_position=self.program_position+3

    def false_skip_code(self, params):
        if params[0] == 0:
            self.program_position=params[1]
        else:
            self.program_position=self.program_position+3

    def less_than_code(self, params):
        if params[0] < params[1]:
            self.program[params[2]]=1
        else:
            self.program[params[2]]=0

    def equals_code(self, params):
        if params[0] == params[1]:
            self.program[params[2]]=1
        else:
            self.program[params[2]]=0

    @staticmethod
    def get_param_modes(number):
        return list(reversed([int(d) for d in str(number)][:-2]))

    @staticmethod
    def read_opcode(code):
        return int(str(code)[-2:])

    def get_opcode_props(self, key):
        opcode_props = {
            1: {'opcode': 1, 'param_count': 3, 'next_instr_step':4, 'write_params':[2], 'function': self.add_code},
            2: {'opcode': 2, 'param_count': 3, 'next_instr_step':4, 'write_params':[2],  'function': self.mult_code},
            3: {'opcode': 3, 'param_count': 1, 'next_instr_step':2, 'write_params':[0],  'function': self.input_code},
            4: {'opcode': 4, 'param_count': 1, 'next_instr_step':2, 'write_params':[0],  'function': self.output_code},
            5: {'opcode': 5, 'param_count': 2, 'next_instr_step':0, 'write_params':[],  'function': self.true_skip_code},
            6: {'opcode': 6, 'param_count': 2, 'next_instr_step':0, 'write_params':[],  'function': self.false_skip_code},
            7: {'opcode': 7, 'param_count': 3, 'next_instr_step':4, 'write_params':[2],  'function': self.less_than_code},
            8: {'opcode': 8, 'param_count': 3, 'next_instr_step':4, 'write_params':[2],  'function': self.equals_code}
        }
        return opcode_props[int(key)]

    @staticmethod
    def pad_param_modes(orig, length):
        while len(orig) < length:
            orig.append(0)
        return orig

machine=Machine(program,[5],0,0)
machine.run_program()