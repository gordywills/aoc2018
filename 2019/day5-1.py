with open("day5.txt", "r") as fd:
    program = fd.read()
program = program.split(",")
program = [int(x) for x in program]


class Machine:
    program = []
    program_position = 0
    input_data = []
    input_position = 0

    def __init__(self, program, input_data, program_position=0, input_position=0):
        self.program = program
        self.input_data = input_data
        self.program_position = program_position
        self.input_position = input_position

    def run_program(self):
        while self.program[self.program_position] != 99:
            position = self.program[self.program_position]
            opcode = self.read_opcode(position)
            props = self.get_opcode_props(opcode)
            param_modes = self.get_param_modes(position)
            param_modes = self.pad_param_modes(param_modes, props['param_count'])
            props['function'](param_modes)
            self.program_position = self.program_position + props['param_count'] + 1

    def add_code(self, param_modes):
        arg1, arg2 = self._add_mult_prelim(param_modes)
        self.program[self.program[self.program_position + 3]] = arg1 + arg2

    def _add_mult_prelim(self, param_modes):
        if param_modes[0] == 1:
            arg1 = self.program[self.program_position + 1]
        else:
            arg1 = self.program[self.program[self.program_position + 1]]
        if param_modes[1] == 1:
            arg2 = self.program[self.program_position + 2]
        else:
            arg2 = self.program[self.program[self.program_position + 2]]
        return arg1, arg2

    def mult_code(self, param_modes):
        arg1, arg2 = self._add_mult_prelim(param_modes)
        self.program[self.program[self.program_position + 3]] = arg1 * arg2

    def input_code(self, param_modes):
        self.program[self.program[self.program_position+1]] = self.input_data[self.input_position]
        self.input_position+=1

    def output_code(self, param_modes):
        if param_modes[0] == 1:
            print(self.program[self.program_position+1])
        else:
            print(self.program[self.program[self.program_position + 1]])

    @staticmethod
    def get_param_modes(number):
        return list(reversed([int(d) for d in str(number)][:-2]))

    @staticmethod
    def read_opcode(code):
        return int(str(code)[-2:])

    def get_opcode_props(self, key):
        opcode_props = {
            1: {'opcode': 1, 'param_count': 3, 'function': self.add_code},
            2: {'opcode': 2, 'param_count': 3, 'function': self.mult_code},
            3: {'opcode': 3, 'param_count': 1, 'function': self.input_code},
            4: {'opcode': 4, 'param_count': 1, 'function': self.output_code}
        }
        return opcode_props[int(key)]

    @staticmethod
    def pad_param_modes(orig, length):
        while len(orig) < length:
            orig.append(0)
        return orig

machine=Machine(program,[1],0,0)
machine.run_program()