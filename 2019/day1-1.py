import math

print(sum(math.floor((int(x.strip()) / 3)) - 2 for x in open("Day1.txt", "r")))
