import math

module_fuel_list = [math.floor((int(x.strip()) / 3)) - 2 for x in open("Day1.txt", "r")]
total_fuel = 0
for module_fuel in module_fuel_list:
    this_fuel = module_fuel
    while this_fuel > 0:
        total_fuel += this_fuel
        this_fuel = math.floor((int(this_fuel) / 3)) - 2
print(total_fuel)
