with open("day2.txt", "r") as fd:
    opcodes = fd.read()
opcodes = opcodes.split(",")
opcodes = [int(x) for x in opcodes]
position = 0

opcodes[1] = 12
opcodes[2] = 2

while opcodes[position] != 99:
    if opcodes[position] == 1:
        opcodes[opcodes[position + 3]] = opcodes[opcodes[position + 1]] + opcodes[opcodes[position + 2]]
    if opcodes[position] == 2:
        opcodes[opcodes[position + 3]] = opcodes[opcodes[position + 1]] * opcodes[opcodes[position + 2]]
    position = position + 4

print(opcodes[0])
