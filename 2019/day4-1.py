lower_bound = 193651
upper_bound = 649729


def double_chars(number):
    chars = list(str(number))
    for x in range(0, len(chars) - 1):
        if chars[x] == chars[x + 1]:
            return True
    return False


def increasing_digits(number):
    chars = list(str(number))
    for x in range(0, len(chars) - 1):
        if int(chars[x]) > int(chars[x + 1]):
            return False
    return True


candidates = []
for candidate in range(lower_bound, upper_bound + 1):
    if double_chars(candidate) and increasing_digits(candidate):
        candidates.append(candidate)

print(len(candidates))
