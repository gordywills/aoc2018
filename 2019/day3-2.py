with open("day3.txt", "r") as fd:
    wire1 = fd.readline()
    wire2 = fd.readline()
wire1 = wire1.split(",")
wire2 = wire2.split(",")


def findextents(wire):
    left_most = 0
    right_most = 0
    up_most = 0
    down_most = 0
    lr = 0
    ud = 0
    for instr in wire:
        if instr[0] == "R":
            lr = lr + int(instr[1:])
            right_most = max(right_most, lr)
        if instr[0] == "L":
            lr = lr - int(instr[1:])
            left_most = min(left_most, lr)
        if instr[0] == "U":
            ud = ud + int(instr[1:])
            up_most = max(up_most, ud)
        if instr[0] == "D":
            ud = ud - int(instr[1:])
            down_most = min(down_most, ud)
    return {"r": right_most, "l": left_most, "u": up_most, "d": down_most}


def plot_wire(wire, number):
    location = (0, 0)
    global interchange_dist
    step_counter = 0
    for instr in wire:
        direction = instr[0]
        length = int(instr[1:])
        if direction == 'R':
            start = location[0] + 1
            end = location[0] + length + 1
            location = (location[0] + length, location[1])
            for x in range(start, end):
                step_counter += 1
                if wire_map[x][location[1]] == ".":
                    wire_map[x][location[1]] = {number: step_counter}
                elif type(wire_map[x][location[1]]) is dict:
                    if number in wire_map[x][location[1]].keys():
                        continue
                    wire_map[x][location[1]][number] = step_counter
                    interchange_dist = min(interchange_dist,
                                           wire_map[x][location[1]]["1"] + wire_map[x][location[1]]["2"])
        if direction == 'L':
            end = location[0]
            start = location[0] - length
            location = (location[0] - length, location[1])
            for x in reversed(range(start, end)):
                step_counter += 1
                if wire_map[x][location[1]] == ".":
                    wire_map[x][location[1]] = {number: step_counter}
                elif type(wire_map[x][location[1]]) is dict:
                    if number in wire_map[x][location[1]].keys():
                        continue
                    wire_map[x][location[1]][number] = step_counter
                    interchange_dist = min(interchange_dist,
                                           wire_map[x][location[1]]["1"] + wire_map[x][location[1]]["2"])
        if direction == 'U':
            start = location[1] + 1
            end = location[1] + length + 1
            location = (location[0], location[1] + length)
            for y in range(start, end):
                step_counter += 1
                if wire_map[location[0]][y] == ".":
                    wire_map[location[0]][y] = {number: step_counter}
                elif type(wire_map[location[0]][y]) is dict:
                    if number in wire_map[location[0]][y].keys():
                        continue
                    wire_map[location[0]][y][number] = step_counter
                    interchange_dist = min(interchange_dist,
                                           wire_map[location[0]][y]["1"] + wire_map[location[0]][y]["2"])
        if direction == 'D':
            end = location[1]
            start = location[1] - length
            location = (location[0], location[1] - length)
            for y in reversed(range(start, end)):
                step_counter += 1
                if wire_map[location[0]][y] == ".":
                    wire_map[location[0]][y] = {number: step_counter}
                elif type(wire_map[location[0]][y]) is dict:
                    if number in wire_map[location[0]][y].keys():
                        continue
                    wire_map[location[0]][y][number] = step_counter
                    interchange_dist = min(interchange_dist,
                                           wire_map[location[0]][y]["1"] + wire_map[location[0]][y]["2"])


wire1_extents = findextents(wire1)
wire2_extents = findextents(wire2)
extents = {
    "r": max(wire1_extents["r"], wire2_extents["r"]),
    "l": min(wire1_extents["l"], wire2_extents["l"]),
    "u": max(wire1_extents["u"], wire2_extents["u"]),
    "d": min(wire1_extents["d"], wire2_extents["d"])}

wire_map_y = {y: "." for y in range(extents["d"] - 2, extents["u"] + 2)}
wire_map = {x: wire_map_y.copy() for x in range(extents["l"] - 2, extents["r"] + 2)}
wire_map[0][0] = 'O'

interchange_dist = (extents["r"] - extents["l"]) * (extents["u"] - extents["d"]) + 2

plot_wire(wire1, "1")
plot_wire(wire2, "2")
print(interchange_dist)
