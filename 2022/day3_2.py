total = 0
priority = "0abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"
with open("day3.txt", "r") as fp:
    sacks = []
    for sack in fp:
        sacks.append(set(sack.strip()))
        if len(sacks) >= 3:
            badge = sacks[0].intersection(sacks[1], sacks[2])
            total += priority.find(list(badge)[0])
            sacks = []
print(total)
