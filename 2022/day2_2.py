ROCK = 'A'
PAPER = 'B'
SCISSORS = 'C'
WIN = 'Z'
DRAW = 'Y'
LOSE = 'X'
SHAPE_SCORE = {ROCK: 1, PAPER: 2, SCISSORS: 3}
ROUND_OUTCOMES = {
    (ROCK, LOSE): (0, SCISSORS),
    (ROCK, DRAW): (3, ROCK),
    (ROCK, WIN): (6, PAPER),
    (PAPER, LOSE): (0, ROCK),
    (PAPER, DRAW): (3, PAPER),
    (PAPER, WIN): (6, SCISSORS),
    (SCISSORS, LOSE): (0, PAPER),
    (SCISSORS, DRAW): (3, SCISSORS),
    (SCISSORS, WIN): (6, ROCK)
}
total = 0
with open("day2.txt", "r") as fd:
    for game_round in fd:
        elf_move, my_outcome = game_round.strip().split(" ")
        total += ROUND_OUTCOMES[elf_move, my_outcome][0] + SHAPE_SCORE[ROUND_OUTCOMES[elf_move, my_outcome][1]]
print(total)
