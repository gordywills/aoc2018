elf_calories = []
with open("day1.txt", "r") as fd:
    running_total = 0
    for x in fd:
        if x.strip() != '':
            running_total = running_total + int(x)
        else:
            elf_calories.append(running_total)
            running_total = 0
ordered = sorted(elf_calories, reverse=True)
print(sum(ordered[0:3]))
