from math import ceil

total = 0
priority = "0abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"
with open("day3.txt", "r") as fp:
    for sack in fp:
        halfway = ceil(len(sack.strip()) / 2)
        error = set(sack.strip()[:halfway]).intersection(set(sack.strip()[halfway:]))
        total += priority.find(list(error)[0])
print(total)
