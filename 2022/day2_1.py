ROCK = 'A'
PAPER = 'B'
SCISSORS = 'C'
TRANSLATE = {'X': ROCK, 'Y': PAPER, 'Z': SCISSORS}
SHAPE_SCORE = {ROCK: 1, PAPER: 2, SCISSORS: 3}
ROUND_OUTCOMES = {
    (ROCK, ROCK): 3,
    (ROCK, PAPER): 6,
    (ROCK, SCISSORS): 0,
    (PAPER, ROCK): 0,
    (PAPER, PAPER): 3,
    (PAPER, SCISSORS): 6,
    (SCISSORS, ROCK): 6,
    (SCISSORS, PAPER): 0,
    (SCISSORS, SCISSORS): 3
}
total = 0
with open("day2.txt", "r") as fd:
    for game_round in fd:
        elf_move, my_move = game_round.strip().split(" ")
        total += ROUND_OUTCOMES[elf_move, TRANSLATE[my_move]] + SHAPE_SCORE[TRANSLATE[my_move]]
print(total)
