duplicate_count = 0
with open("day4.txt", "r") as fp:
    for pair in fp:
        elf1, elf2 = pair.strip().split(",")
        elf1_start, elf1_end = elf1.split("-")
        elf2_start, elf2_end = elf2.split("-")
        elf1_set = set([x for x in range(int(elf1_start), int(elf1_end) + 1)])
        elf2_set = set([x for x in range(int(elf2_start), int(elf2_end) + 1)])
        if elf1_set.intersection(elf2_set) != set():
            duplicate_count += 1
print(duplicate_count)
