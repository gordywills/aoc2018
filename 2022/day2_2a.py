r = {"A X": (4, 3), "A Y": (8, 4), "A Z": (3, 8), "B X": (1, 1), "B Y": (5, 5), "B Z": (9, 9), "C X": (7, 2),
     "C Y": (2, 6), "C Z": (6, 7)}
t, t2 = 0, 0
with open("day2.txt", "r") as fd:
    for gr in fd:
        t += r[gr.strip()][0]
        t2 += r[gr.strip()][1]
print("1: ", t, "\n2: ", t2)
