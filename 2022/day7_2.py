from re import match


def size_recursion(local_root):
    if local_root[3] is not None:
        return local_root[3]
    local_size = 0
    for file_tuple in local_root[2]:
        local_size += file_tuple[0]
    for subdir in local_root[1].values():
        local_size += size_recursion(subdir)
    local_root[3] = local_size
    return local_size


def size_search(target_size, local_root, target):
    if local_root[3] > target_size:
        target = min(target, local_root[3])
    for subdir in local_root[1].values():
        target = min(target, size_search(target_size, subdir, target))
    return target


root = [None, {}, [], None]
cmd_re = r'\$ (..)(.*)'
current_dir = root

with open("day7.txt", "r") as fp:
    for line in fp:
        if line.strip().startswith("$"):
            matches = match(cmd_re, line.strip())
            if matches[1] == 'cd' and matches[2].strip() == '/':
                current_dir = root
                continue
            if matches[1] == 'cd' and matches[2].strip() == "..":
                current_dir = current_dir[0]
                continue
            if matches[1] == 'cd':
                if not matches[2].strip() in current_dir[1]:
                    exit('no such dir')
                current_dir = current_dir[1][matches[2].strip()]
                continue
            if matches[1] == 'ls':
                continue
        else:
            if line.strip().startswith("dir"):
                if line.strip()[4:] in current_dir[1]:
                    exit('dir already exists')
                current_dir[1][line.strip()[4:]] = [current_dir, {}, [], None]
            else:
                size, name = line.strip().split(" ")
                current_dir[2].append((int(size), name))

free = 70000000 - size_recursion(root)
required = 30000000 - free
print(size_search(required, root, 70000000))
