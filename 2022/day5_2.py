from re import match

stacks = {1: [], 2: [], 3: [], 4: [], 5: [], 6: [], 7: [], 8: [], 9: []}
instr_re = r'move (\d*) from (\d) to (\d)'
with open("day5.txt", "r") as fp:
    for line in fp:
        if not line.startswith("move"):
            for pos in range(1, len(line), 4):
                if line[pos] != ' ' and not line[pos].isnumeric():
                    stacks[int(pos / 4) + 1].append(line[pos])
        else:
            heap = []
            instruction = match(instr_re, line)
            for _ in range(int(instruction[1])):
                heap.append(stacks[int(instruction[2])].pop(0))
            for _ in range(int(instruction[1])):
                stacks[int(instruction[3])].insert(0, heap.pop())
answer = ''
for index in range(len(stacks)):
    answer = answer + dict(enumerate(stacks[index + 1])).get(0, "")
print(answer)
