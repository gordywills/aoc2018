from modint import chinese_remainder

with open("day13.txt", "r")as fd:
    target = int(fd.readline())
    id_list = [x for x in fd.readline().split(",")]

mods = []
rems = []
for reminder, bus_id in enumerate(id_list):
    if bus_id == "x":
        continue
    mods.append(int(bus_id))
    rems.append((int(bus_id) - reminder) % int(bus_id))

print(chinese_remainder(mods, rems))
