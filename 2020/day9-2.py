def check_number(target, pool):
    for k, v in enumerate(pool):
        for x, y in enumerate(pool):
            if k == x:
                continue
            if v + y == target:
                return True
    return False


preamble_length = 25
numbers = []
new_target = 0
with open("day9.txt", "r")as fd:
    for line in fd:
        numbers.append(int(line.strip()))

for x in range(preamble_length, len(numbers)):
    target = numbers[x]
    pool = numbers[x - preamble_length:x]
    if not check_number(target, pool):
        new_target = numbers[x]
        break

for k, v in enumerate(numbers):
    for x in range(k, len(numbers)):
        if sum(numbers[k:x]) == new_target:
            print(max(numbers[k:x]) + min(numbers[k:x]))
            exit()
        if sum(numbers[k:x]) > new_target:
            break
