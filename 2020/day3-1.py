map = {}
linecount = 0
with open("day3.txt", "r") as fp:
    for line in fp:
        this_line = []
        for symbol in line.strip():
            this_line.append(symbol)
        map[linecount] = this_line
        linecount = linecount + 1

length = len(map[0])
start_row = 0
col = 0

tree_count = 0

for row in range(len(map) - 1):
    col = (((row + 1) * 3) % length)
    if map[row + 1][col] == "#":
        tree_count = tree_count + 1
print(tree_count)
