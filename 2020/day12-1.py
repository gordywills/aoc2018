class Ship:
    def __init__(self):
        self.faces = "E"
        self.x_coord = 0
        self.y_coord = 0

    def take_step(self, command):
        action = command[0]
        ammount = int(command[1:])
        if action == "N":
            self.y_coord += ammount
        if action == "S":
            self.y_coord -= ammount
        if action == "E":
            self.x_coord += ammount
        if action == "W":
            self.x_coord -= ammount
        if action == "R":
            ordered_points = ["E", "S", "W", "N"]
            steps = (int((ammount / 90)) + ordered_points.index(self.faces)) % 4
            self.faces = ordered_points[steps]
        if action == "L":
            ordered_points = ["E", "N", "W", "S"]
            steps = (int((ammount / 90)) + ordered_points.index(self.faces)) % 4
            self.faces = ordered_points[steps]
        if action == "F":
            self.take_step(self.faces + str(ammount))

    def man_distance(self):
        return abs(self.x_coord) + abs(self.y_coord)


boat = Ship()

with open("day12.txt", "r") as fd:
    for line in fd:
        boat.take_step(line.strip())

print(boat.man_distance())
