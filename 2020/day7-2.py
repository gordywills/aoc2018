import re


def get_total_children(node, rules):
    if node.contains == {}:
        return 0
    return sum([int(v) + (int(v) * get_total_children(rules[k], rules)) for k, v in node.contains.items()])


class Rule:
    name: str
    contains: dict
    parents: list

    def __init__(self, name):
        self.name = name
        self.contains = {}
        self.parents = []

    def __str__(self):
        return self.name + ": " + (
                ",".join([v + " " + k for k, v in self.contains.items()]) + ": " + ",".join(self.parents))

    def add_to_contents(self, name, number):
        self.contains[name] = number

    def add_to_parents(self, name):
        self.parents.append(name)

    def does_this_contain(self, name):
        return name in self.contains

    def populate_parents(self, rules):
        for k, test_rule in rules.items():
            if self.name == k:
                continue
            if test_rule.does_this_contain(self.name):
                self.add_to_parents(k)


expr = re.compile(r"^(.*) bags contain (.*) bags{0,1}\.$")
sub_part_expr = re.compile(r"^(\d+) (.*)$")
rules = {}
with open("day7.txt", "r") as fd:
    for rule in fd:
        parts = expr.findall(rule)
        new_rule = Rule(parts[0][0])
        sub_parts = parts[0][1].replace("s,", ",").split(" bag, ")
        for sub_part in sub_parts:
            if sub_part != "no other":
                elements = sub_part_expr.findall(sub_part)
                new_rule.add_to_contents(elements[0][1], elements[0][0])
        rules[parts[0][0]] = new_rule
for k, x in rules.items():
    x.populate_parents(rules)

target = "shiny gold"
print(get_total_children(rules[target], rules))
