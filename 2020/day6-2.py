import re

with open("day6.txt", "r") as fd:
    data = fd.read()

raw_answers = re.sub(r"(.)\n", "\g<1> ", data).split("\n")

parsed_answers = []
for x in range(len(raw_answers)):
    answers_list = raw_answers[x].split()
    result_list = answers_list[0]
    for y in range(len(answers_list) - 1):
        result_list = list(set(result_list) & set(answers_list[y + 1]))
    parsed_answers.append("".join(sorted(result_list)))

total = 0
for answers in parsed_answers:
    total = total + len(answers)
print(total)
