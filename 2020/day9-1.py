def check_number(target, pool):
    for k, v in enumerate(pool):
        for x, y in enumerate(pool):
            if k == x:
                continue
            if v + y == target:
                return True
    return False


preamble_length = 25
numbers = []
with open("day9.txt", "r")as fd:
    for line in fd:
        numbers.append(int(line.strip()))

for x in range(preamble_length, len(numbers)):
    target = numbers[x]
    pool = numbers[x - preamble_length:x]
    if not check_number(target, pool):
        print(numbers[x])
        exit()
