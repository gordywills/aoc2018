import operator
import re


def calc(seq_list, op, ops):
    idx = seq_list.index(op)
    return str(ops[op](int(add_mult(" ".join(seq_list[:idx]), ops)), int(add_mult(" ".join(seq_list[idx + 1:]), ops))))


def add_mult(sequence, ops):
    seq_list = sequence.split(" ")
    if len(seq_list) == 1:
        return str(seq_list[0])
    return calc(seq_list, "*", ops) if "*" in seq_list else calc(seq_list, "+", ops)


def process_sequence(sequence, ops):
    parts = para_finder.findall(sequence)
    while parts:
        for part in parts:
            sequence = re.sub(r"\(" + re.escape(part) + r"\)", add_mult(part, ops), sequence, 1)
        parts = para_finder.findall(sequence)
    return add_mult(sequence, ops)


para_finder = re.compile(r"\(([\d +*]*?)\)")
operations = {"+": operator.add, "*": operator.mul}
with open("day18.txt", "r")as fd:
    print(sum([int(process_sequence(line.strip(), operations)) for line in fd]))
