class Seat:
    row: int
    col: int
    state: str
    next_state: str
    stable: bool

    def __init__(self, row, col, state):
        self.row = row
        self.col = col
        self.state = state
        self.next_state = ""
        self.stable = False

    def get_state(self):
        return self.state

    def transition(self):
        if self.state == self.next_state:
            self.stable = True
        else:
            self.stable = False
        self.state = self.next_state

    def set_next_state(self, adjacent_list):
        if self.state == ".":
            self.next_state = "."
        if self.state == "L":
            occupied = False
            for adj_seat in adjacent_list:
                if adj_seat.get_state() == "#":
                    occupied = True
            if occupied:
                self.next_state = "L"
            else:
                self.next_state = "#"
        if self.state == "#":
            counter = 0
            for adj_seat in adjacent_list:
                if adj_seat.get_state() == "#":
                    counter += 1
            if counter >= 4:
                self.next_state = "L"
            else:
                self.next_state = "#"

    @classmethod
    def check_stable(cls, seat_list):
        for x in seat_list:
            for y in x:
                if not y.stable:
                    return False
        return True

    @classmethod
    def prepare_transition(cls, seat_list):
        for x in seat_list:
            for y in x:
                y.set_next_state(cls.get_adjacent(y.row, y.col, seat_list))

    @classmethod
    def do_transition(cls, seat_list):
        for x in seat_list:
            for y in x:
                y.transition()

    @classmethod
    def get_adjacent(cls, row, col, seat_list):
        top = row - 1 if row - 1 > 0 else 0
        bottom = row + 1 if row + 1 < len(seat_list) else len(seat_list) - 1
        left = col - 1 if col - 1 > 0 else 0
        right = col + 1 if col + 1 < len(seat_list[0]) else len(seat_list[0]) - 1
        rows = seat_list[top:bottom + 1]

        adjacent_list_md = [seat_row[left:right + 1] for seat_row in rows]
        adjacent_list = []
        for line in adjacent_list_md:
            for adj_seat in line:
                if adj_seat.row == row and adj_seat.col == col:
                    continue
                adjacent_list.append(adj_seat)
        return adjacent_list

    def __str__(self):
        return self.state


with open("day11.txt", "r") as fd:
    seats = [[Seat(row, col, char) for col, char in enumerate(line.strip())] for row, line in enumerate(fd)]

while not Seat.check_stable(seats):
    Seat.prepare_transition(seats)
    Seat.do_transition(seats)

    for line in seats:
        print("".join([str(char) for char in line]))
    print()

count = 0
for line in seats:
    for seat in line:
        if seat.get_state() == "#":
            count += 1
print(count)
