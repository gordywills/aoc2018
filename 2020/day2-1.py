input_list = []
with open("day2.txt", "r") as fd:
    for x in fd:
        parts = x.split(":")
        polparts = parts[0].split()
        maxmin = polparts[0].split("-")
        input_list.append([maxmin[0], maxmin[1], polparts[1], parts[1].strip()])

count = 0
for question in input_list:
    occurrences = question[3].count(question[2])
    if int(question[0]) <= occurrences <= int(question[1]):
        count = count + 1

print(count)
