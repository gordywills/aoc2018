adapter_list = []
with open("day10.txt", "r")as fd:
    for line in fd:
        adapter_list.append(int(line.strip()))

adapter_list = sorted(adapter_list)
adapter_list.insert(0, 0)
adapter_list.append(max(adapter_list) + 3)

branch_dict = {}
for adapter in adapter_list:
    if adapter == 0:
        branch_dict[0] = 1
        continue
    first = branch_dict[adapter - 1] if adapter - 1 in branch_dict else 0
    second = branch_dict[adapter - 2] if adapter - 2 in branch_dict else 0
    third = branch_dict[adapter - 3] if adapter - 3 in branch_dict else 0
    branch_dict[adapter] = first + second + third

print(branch_dict[max(branch_dict)])
