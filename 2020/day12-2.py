class Ship:
    def __init__(self):
        self.faces = "E"
        self.x_coord = 0
        self.y_coord = 0
        self.wp_x_coord = 10
        self.wp_y_coord = 1

    def take_step(self, command):
        action = command[0]
        ammount = int(command[1:])
        if action == "N":
            self.wp_y_coord += ammount
        if action == "S":
            self.wp_y_coord -= ammount
        if action == "E":
            self.wp_x_coord += ammount
        if action == "W":
            self.wp_x_coord -= ammount
        if (action == "R" or action == "L") and int((ammount / 90)) % 4 in [0, 2]:
            self.wp_x_coord, self.wp_y_coord = -self.wp_x_coord, -self.wp_y_coord
        if (action == "R" and int((ammount / 90)) % 4 == 1) or (action == "L" and int((ammount / 90)) % 4 == 3):
            if self.wp_x_coord >= 0 and self.wp_y_coord > 0:
                self.wp_x_coord, self.wp_y_coord = abs(self.wp_y_coord), -abs(self.wp_x_coord)
            elif self.wp_x_coord > 0 and self.wp_y_coord <= 0:
                self.wp_x_coord, self.wp_y_coord = -abs(self.wp_y_coord), -abs(self.wp_x_coord)
            elif self.wp_x_coord <= 0 and self.wp_y_coord < 0:
                self.wp_x_coord, self.wp_y_coord = -abs(self.wp_y_coord), abs(self.wp_x_coord)
            elif self.wp_x_coord < 0 and self.wp_y_coord >= 0:
                self.wp_x_coord, self.wp_y_coord = abs(self.wp_y_coord), abs(self.wp_x_coord)
        if (action == "L" and int((ammount / 90)) % 4 == 1) or (action == "R" and int((ammount / 90)) % 4 == 3):
            if self.wp_x_coord >= 0 and self.wp_y_coord > 0:
                self.wp_x_coord, self.wp_y_coord = -abs(self.wp_y_coord), abs(self.wp_x_coord)
            elif self.wp_x_coord > 0 and self.wp_y_coord <= 0:
                self.wp_x_coord, self.wp_y_coord = abs(self.wp_y_coord), abs(self.wp_x_coord)
            elif self.wp_x_coord <= 0 and self.wp_y_coord < 0:
                self.wp_x_coord, self.wp_y_coord = abs(self.wp_y_coord), -abs(self.wp_x_coord)
            elif self.wp_x_coord < 0 and self.wp_y_coord >= 0:
                self.wp_x_coord, self.wp_y_coord = -abs(self.wp_y_coord), -abs(self.wp_x_coord)
        if action == "F":
            self.x_coord += ammount * self.wp_x_coord
            self.y_coord += ammount * self.wp_y_coord

    def man_distance(self):
        return abs(self.x_coord) + abs(self.y_coord)


boat = Ship()

with open("day12.txt", "r") as fd:
    for line in fd:
        boat.take_step(line.strip())

print(boat.man_distance())
