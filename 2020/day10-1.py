adapter_list = []
with open("day10.txt", "r")as fd:
    for line in fd:
        adapter_list.append(int(line.strip()))

adapter_list = sorted(adapter_list)
adapter_list.insert(0, 0)
adapter_list.append(max(adapter_list) + 3)

diffs = {1: 0, 2: 0, 3: 0}
for x in range(len(adapter_list) - 1):
    diff = adapter_list[x + 1] - adapter_list[x]
    diffs[diff] += 1

print(diffs)
print(diffs[1] * diffs[3])
