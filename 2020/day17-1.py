import math


class Cube:
    def __init__(self, state):
        self.state = state
        self.next_state = ""

    def transition(self):
        if self.next_state == "":
            raise ValueError
        self.state = self.next_state
        self.next_state = ""

    def get_state(self):
        return self.state

    def set_state(self, state):
        self.state = state

    def set_next_state(self, state):
        self.next_state = state


grid = {0: {}}
with open("day17.txt", "r") as fd:
    row = -math.floor(len(fd.readlines()) / 2)
    fd.seek(0)
    for row in range(row, -1 * row + 1):
        line = fd.readline().strip()
        if row not in grid[0]:
            grid[0][row] = {}
        col = -math.floor(len(line) / 2)
        for x in line:
            grid[0][row][col] = Cube(x)
            col += 1

for z in grid:
    for y in grid[z]:
        for x in grid[z][y]:
            if z == max(grid.keys()) or min(grid.keys()):
                pass

# check outer layers
# add outer layer if required
# loop each poistion and set next state
##determine adjacent cubes
##count active nd inactive of adjacent cubes
# loop each position and transition
