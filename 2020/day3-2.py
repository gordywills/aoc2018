map = {}
linecount = 0
with open("day3.txt", "r") as fp:
    for line in fp:
        this_line = []
        for symbol in line.strip():
            this_line.append(symbol)
        map[linecount] = this_line
        linecount = linecount + 1

length = len(map[0])


def count_trees(roffset, doffset):
    tree_count = 0
    col = 0
    for row in range(0, len(map) - 1, doffset):
        col = ((col + roffset) % length)
        if map[row + doffset][col] == "#":
            tree_count = tree_count + 1
    return tree_count


product = (
        count_trees(1, 1) *
        count_trees(3, 1) *
        count_trees(5, 1) *
        count_trees(7, 1) *
        count_trees(1, 2)
)
print(product)
