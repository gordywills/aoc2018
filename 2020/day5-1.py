raw_passes = []
with open("day5.txt", "r") as fp:
    for line in fp:
        raw_passes.append(line.strip())


def calculate_number(code, base_length):
    possible_number = [x for x in range(base_length)]
    for char in code:
        if char == "F" or char == "L":
            possible_number = possible_number[:int(len(possible_number) / 2)]
        if char == "B" or char == "R":
            possible_number = possible_number[int(len(possible_number) / 2):]
    return possible_number[0]


seats_occupado = []
for raw_pass in raw_passes:
    row = calculate_number(raw_pass[:7], 128)
    col = calculate_number(raw_pass[7:], 8)
    seats_occupado.append((row * 8) + col)

print(max(seats_occupado))
