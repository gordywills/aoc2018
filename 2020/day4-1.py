import re

with open("day4.txt", "r") as fd:
    data = fd.read()

raw_passports = re.sub(r"(.)\n", "\g<1> ", data).split("\n")
passports = []
for raw_passport in raw_passports:
    this_passport = {k: v for k, v in [pair.split(":") for pair in raw_passport.split()]}
    passports.append(this_passport)
valid_count = 0
for passport in passports:
    if ("byr" in passport and
            "iyr" in passport and
            "eyr" in passport and
            "hgt" in passport and
            "hcl" in passport and
            "ecl" in passport and
            "pid" in passport
    ):
        valid_count = valid_count + 1

print(valid_count)
