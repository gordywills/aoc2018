import re
from collections import defaultdict


class Rules:
    def __init__(self):
        self.rules_collection = defaultdict(list)
        self.rule_regex = re.compile(r"^(.*): (\d+)-(\d+) or (\d+)-(\d+)$")

    def add_rule(self, name, number_list):
        self.rules_collection[name] = number_list

    def decode_rule(self, rule_str):
        parts = self.rule_regex.findall(rule_str)
        first_list = [x for x in range(int(parts[0][1]), int(parts[0][2]) + 1)]
        second_list = [x for x in range(int(parts[0][3]), int(parts[0][4]) + 1)]
        self.add_rule(parts[0][0], first_list + second_list)

    def get_combined_list(self):
        combined_lst = []
        for k, v in self.rules_collection.items():
            combined_lst = combined_lst + v
        return list(set(combined_lst))

    def get_avavilable_rules(self):
        return [x for x in self.rules_collection.keys()]

    def get_valid_list_for_rule(self, rule):
        return self.rules_collection[rule]


class Ticket:
    def __init__(self, number_list, rules):
        self.values = [int(x) for x in number_list]
        self.rules = rules

    def get_invalid_numbers(self):
        valid_list = self.rules.get_combined_list()
        invalid_numbers = [x for x in self.values if x not in valid_list]
        return invalid_numbers


blank_count = 0
my_rules = Rules()
nearby_tickets = []
with open("day16.txt", "r") as fd:
    for line in fd:
        if line.strip() == "":
            blank_count += 1
            continue
        if blank_count == 0:
            my_rules.decode_rule(line.strip())
            continue
        if blank_count == 1:
            if line.startswith("your"):
                continue
            my_ticket = Ticket(line.strip().split(","), my_rules)
        if blank_count == 2:
            if line.startswith("nearby"):
                continue
            this_ticket = Ticket(line.strip().split(","), my_rules)
            nearby_tickets.append(this_ticket)

nearby_tickets = [x for x in nearby_tickets if x.get_invalid_numbers() == []]
options_per_position = defaultdict(list)
cross_map = {position: [] for position in range(len(my_ticket.values))}

for ticket in [my_ticket] + nearby_tickets:
    for k, v in enumerate(ticket.values):
        cross_map[k].append(v)

for position, mapped_values in cross_map.items():
    for option in my_rules.get_avavilable_rules():
        invalid_numbers = [x for x in mapped_values if x not in my_rules.get_valid_list_for_rule(option)]
        if not invalid_numbers:
            options_per_position[position].append(option)

options_per_position = {k: v for k, v in sorted(options_per_position.items(), key=lambda v: len(v[1]))}

used = []
position_map = {}
for position, options in options_per_position.items():
    for option in options:
        if option not in used:
            used.append(option)
            position_map[position] = option
            break
total = 1
position_map = {v: k for k, v in position_map.items()}
for name, position in position_map.items():
    if name.startswith("departure"):
        total = total * my_ticket.values[position]
print(total)
