with open("day13.txt", "r")as fd:
    target = int(fd.readline())
    id_list = [int(y) for y in [x for x in fd.readline().split(",") if x != "x"]]

try_time = target
while True:
    for x in id_list:
        if try_time % x == 0:
            print(x * (try_time - target))
            exit()
    try_time += 1
