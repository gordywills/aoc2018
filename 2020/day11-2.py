class Seat:
    row: int
    col: int
    state: str
    next_state: str
    stable: bool

    def __init__(self, row, col, state):
        self.row = row
        self.col = col
        self.state = state
        self.next_state = ""
        self.stable = False

    def get_state(self):
        return self.state

    def transition(self):
        if self.state == self.next_state:
            self.stable = True
        else:
            self.stable = False
        self.state = self.next_state

    def set_next_state(self, adjacent_list):
        if self.state == ".":
            self.next_state = "."
        if self.state == "L":
            occupied = False
            for adj_seat in adjacent_list:
                if adj_seat.get_state() == "#":
                    occupied = True
            if occupied:
                self.next_state = "L"
            else:
                self.next_state = "#"
        if self.state == "#":
            counter = 0
            for adj_seat in adjacent_list:
                if adj_seat.get_state() == "#":
                    counter += 1
            if counter >= 5:
                self.next_state = "L"
            else:
                self.next_state = "#"

    @classmethod
    def check_stable(cls, seat_list):
        for x in seat_list:
            for y in x:
                if not y.stable:
                    return False
        return True

    @classmethod
    def prepare_transition(cls, seat_list):
        for x in seat_list:
            for y in x:
                y.set_next_state(cls.get_adjacent(y.row, y.col, seat_list))

    @classmethod
    def do_transition(cls, seat_list):
        for x in seat_list:
            for y in x:
                y.transition()

    @classmethod
    def get_adjacent(cls, row, col, seat_list):
        adjacent_list = []
        # go left til i hit something
        for left_col in reversed(range(col)):
            if seat_list[row][left_col].get_state() != ".":
                adjacent_list.append(seat_list[row][left_col])
                break
        # go right
        for right_col in range(col + 1, len(seat_list[row])):
            if seat_list[row][right_col].get_state() != ".":
                adjacent_list.append(seat_list[row][right_col])
                break
        # go up
        for up_row in reversed(range(row)):
            if seat_list[up_row][col].get_state() != ".":
                adjacent_list.append(seat_list[up_row][col])
                break
        # go down
        for down_row in range(row + 1, len(seat_list)):
            if seat_list[down_row][col].get_state() != ".":
                adjacent_list.append(seat_list[down_row][col])
                break
        # go up left
        y = row - 1
        x = col - 1
        while x >= 0 and y >= 0:
            if seat_list[y][x].get_state() != ".":
                adjacent_list.append(seat_list[y][x])
                break
            x -= 1
            y -= 1
        # go up and right
        y = row - 1
        x = col + 1
        while x < len(seat_list[y]) and y >= 0:
            if seat_list[y][x].get_state() != ".":
                adjacent_list.append(seat_list[y][x])
                break
            x += 1
            y -= 1
        # go down and left
        y = row + 1
        x = col - 1
        while x >= 0 and y < len(seat_list):
            if seat_list[y][x].get_state() != ".":
                adjacent_list.append(seat_list[y][x])
                break
            x -= 1
            y += 1
        # go down and right
        y = row + 1
        x = col + 1
        while y < len(seat_list) and x < len(seat_list[y]):
            if seat_list[y][x].get_state() != ".":
                adjacent_list.append(seat_list[y][x])
                break
            x += 1
            y += 1

        return adjacent_list

    def __str__(self):
        return self.state


with open("day11.txt", "r") as fd:
    seats = [[Seat(row, col, char) for col, char in enumerate(line.strip())] for row, line in enumerate(fd)]

while not Seat.check_stable(seats):
    Seat.prepare_transition(seats)
    Seat.do_transition(seats)

    for line in seats:
        print("".join([str(char) for char in line]))
    print()

count = 0
for line in seats:
    for seat in line:
        if seat.get_state() == "#":
            count += 1
print(count)
