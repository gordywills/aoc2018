raw_input = [18, 11, 9, 0, 5, 1]
target_iterations = 30000000
turn = len(raw_input) + 1
last_spoken = raw_input[-1]
turns_dict = {k: [x + 1] for x, k in enumerate(raw_input)}
while turn <= target_iterations:
    if len(turns_dict[last_spoken]) == 1:
        last_spoken = 0
    else:
        last_spoken = turns_dict[last_spoken][0] - turns_dict[last_spoken][1]
    if last_spoken not in turns_dict:
        turns_dict[last_spoken] = [turn]
    else:
        turns_dict[last_spoken] = [turn, turns_dict[last_spoken][0]]
    turn += 1
print(last_spoken)
