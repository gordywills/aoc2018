import re


class BootComputer:
    def __init__(self, code):
        self.accumulator = 0
        self.pointer = 0
        self.regex = re.compile(r"^(.{3}) (.*)$")
        self.code = code
        self.code_length = len(self.code)
        self.jmp_count, self.nop_count = self.count_jmp_nop()

    def command_executor(self, cmd, val):
        if cmd == "nop":
            self.pointer = self.pointer + 1
            return
        if cmd == "acc":
            self.accumulator = self.accumulator + val
            self.pointer = self.pointer + 1
            return
        if cmd == "jmp":
            self.pointer = self.pointer + val

    def command_decoder(self, cmd_string):
        parts = self.regex.findall(cmd_string)
        return parts[0][0], int(parts[0][1])

    def count_jmp_nop(self):
        jmp = 0
        nop = 0
        for index, instr in self.code.items():
            parts = self.regex.findall(instr)
            if parts[0][0] == "jmp":
                jmp += 1
            if parts[0][0] == "nop":
                nop += 1
        return jmp, nop

    def execute_with_deets(self, count, string, subs):
        counter = 0
        while counter <= count:
            executed_list = []
            self.accumulator = 0
            self.pointer = 0
            inner_counter = 0
            while self.pointer not in executed_list:
                executed_list.append(self.pointer)
                cmd, val = self.command_decoder(self.code[self.pointer])
                if cmd == string:
                    if counter == inner_counter:
                        cmd = subs
                    inner_counter += 1
                self.command_executor(cmd, val)
                if self.pointer >= self.code_length:
                    print(self.accumulator)
                    exit()
            counter += 1

    def execute(self):
        self.execute_with_deets(self.jmp_count, "jmp", "nop")
        self.execute_with_deets(self.nop_count, "nop", "jmp")


code_list = {}
with open("day8.txt", "r")as fd:
    for index, instruction in enumerate(fd):
        code_list[index] = instruction.strip()

comp = BootComputer(code_list)
comp.execute()
