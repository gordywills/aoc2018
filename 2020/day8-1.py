import re


class BootComputer:
    def __init__(self, code):
        self.accumulator = 0
        self.pointer = 0
        self.regex = re.compile(r"^(.{3}) (.*)$")
        self.code = code

    def command_executor(self, cmd, val):
        if cmd == "nop":
            self.pointer = self.pointer + 1
            return
        if cmd == "acc":
            self.accumulator = self.accumulator + val
            self.pointer = self.pointer + 1
            return
        if cmd == "jmp":
            self.pointer = self.pointer + val

    def command_decoder(self, cmd_string):
        parts = self.regex.findall(cmd_string)
        return parts[0][0], int(parts[0][1])

    def execute(self):
        executed_list = []
        while self.pointer not in executed_list:
            executed_list.append(self.pointer)
            cmd, val = self.command_decoder(self.code[self.pointer])
            self.command_executor(cmd, val)
        print(self.accumulator)


code_list = {}
with open("day8.txt", "r")as fd:
    for index, instruction in enumerate(fd):
        code_list[index] = instruction.strip()

comp = BootComputer(code_list)
comp.execute()
