raw_input = [18, 11, 9, 0, 5, 1]
turn = len(raw_input)
while turn < 2020:
    turn += 1
    tail = raw_input[-1]
    if tail not in raw_input[:-1]:
        raw_input.append(0)
        continue
    age = [x for x in reversed(raw_input[:-1])].index(tail) + 1
    raw_input.append(age)
print(raw_input[-1])
