class MemoryMachine:
    def __init__(self):
        self.mask = ""
        self.mem_store = {}

    def update_mask(self, new_mask):
        self.mask = new_mask

    def convert_dec_to_bin(self, dec_to_convert):
        return str(bin(int(dec_to_convert)))[2:].zfill(36)

    def convert_bin_to_dec(self, bin_to_convert):
        return int(bin_to_convert, 2)

    def update_mem(self, address, value):
        self.mem_store[address] = self.apply_mask(self.convert_dec_to_bin(value))

    def apply_mask(self, bin_string):
        mask_list = list(self.mask)
        value_list = list(bin_string)
        new_list = []
        for key, bit in enumerate(mask_list):
            if bit == "X":
                new_list.append(value_list[key])
            if bit == "1":
                new_list.append("1")
            if bit == "0":
                new_list.append("0")
        return "".join(new_list)

    def get_store_total(self):
        total = 0
        for k, item in self.mem_store.items():
            total += self.convert_bin_to_dec(item)
        return total


store = MemoryMachine()
with open("day14.txt", "r") as fd:
    for line in fd:
        if line.startswith("mask"):
            store.update_mask(line.strip()[-36:])
        if line.startswith("mem["):
            start = 4
            end = line.index("]")
            int_start = line.index("=")
            store.update_mem(line[start:end], line.strip()[int_start + 1:])

print(store.get_store_total())
