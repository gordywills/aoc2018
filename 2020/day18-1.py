import re

para_finder = re.compile(r"\(([\d +\*]*?)\)")


def add_mult(sequence):
    seq_list = sequence.split(" ")
    if len(seq_list) == 1: return str(seq_list[0])
    total = int(seq_list[-1])
    operator = seq_list[-2]
    if operator == "+":
        total = total + int(add_mult(" ".join(seq_list[:-2])))
    if operator == "*":
        total = total * int(add_mult(" ".join(seq_list[:-2])))
    return str(total)


def process_sequence(sequence):
    parts = para_finder.findall(sequence)
    while parts:
        for part in parts:
            sequence = re.sub(r"\(" + re.escape(part) + r"\)", add_mult(part), sequence, 1)
        parts = para_finder.findall(sequence)
    return add_mult(sequence)


totals_list = []
with open("day18.txt", "r")as fd:
    for line in fd:
        totals_list.append(process_sequence(line.strip()))
print(sum([int(x) for x in totals_list]))
