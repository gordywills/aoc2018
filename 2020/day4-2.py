import re

with open("day4.txt", "r") as fd:
    data = fd.read()

raw_passports = re.sub(r"(.)\n", "\g<1> ", data).split("\n")
passports = []
for raw_passport in raw_passports:
    this_passport = {k: v for k, v in [pair.split(":") for pair in raw_passport.split()]}
    passports.append(this_passport)
valid_count = 0
for passport in passports:
    valid = False
    if ("byr" in passport and
            "iyr" in passport and
            "eyr" in passport and
            "hgt" in passport and
            "hcl" in passport and
            "ecl" in passport and
            "pid" in passport):
        valid = True
        if int(passport["byr"]) < 1920 or int(passport["byr"]) > 2002:
            valid = False
        if int(passport["iyr"]) < 2010 or int(passport["iyr"]) > 2020:
            valid = False
        if int(passport["eyr"]) < 2020 or int(passport["eyr"]) > 2030:
            valid = False
        if (passport["hgt"][-2:] == "cm" and (int(passport["hgt"][:-2]) < 150 or int(passport["hgt"][:-2]) > 193)) or (
                passport["hgt"][-2:] == "in" and (
                int(passport["hgt"][:-2]) < 59 or int(passport["hgt"][:-2]) > 76)) or (
                passport["hgt"][-2:] != "cm" and passport["hgt"][-2:] != "in"):
            valid = False
        if re.match(r"^#[0-9,a-f]{6}$", passport["hcl"]) is None:
            valid = False
        if passport["ecl"] not in ["amb", "blu", "brn", "gry", "grn", "hzl", "oth"]:
            valid = False
        if re.match(r"^[0-9]{9}$", passport["pid"]) is None:
            valid = False
    if valid:
        valid_count = valid_count + 1
print(valid_count)
