import re

with open("day6.txt", "r") as fd: print(
    sum([len(list(set(list(answer)))) for answer in re.sub(r"(.)\n", "\g<1>", fd.read()).split("\n")]))
